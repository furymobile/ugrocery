//
//  IKOPriceUpdateDataStore.h
//  uGrocery
//
//  Created by Duane Schleen on 11/5/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKODataStore.h"
#import "IKOPriceUpdate.h"

@interface IKOPriceUpdateDataStore : IKODataStore

- (void)sendPriceUpdate:(IKOPriceUpdate *)priceUpdate;
- (void)retryResubmissions;

@end
