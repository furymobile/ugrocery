//
//  IKOMyProductsDataStore.h
//  uGrocery
//
//  Created by Duane Schleen on 8/12/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKODataStore.h"
#import "IKOFavoriteItem.h"

@interface IKOFavoritesDataStore : IKODataStore

- (BOOL)saveMyProductItem:(IKOFavoriteItem **)item;
- (IKOFavoriteItem *)getMyProductItemForProductId:(NSNumber *)productId;
- (BOOL)removeMyProductItem:(IKOFavoriteItem *)item;
- (NSArray *)allObjects;

@end
