//
//  Copyright (c) 2014 Fury Mobile. All rights reserved.
//
//  License:
//
//  Apache License
//  Version 2.0, January 2004
//  http://www.apache.org/licenses/
//

#import "Helium.h"

NSString * const HeliumDomain = @"com.furymobile.helium";

#define EmptyKeyError   [NSError errorWithDomain:HeliumDomain code:0 userInfo:@{NSLocalizedDescriptionKey : @"Empty Key"}];
#define EmptyValueError [NSError errorWithDomain:HeliumDomain code:0 userInfo:@{NSLocalizedDescriptionKey : @"Empty Value"}]

@implementation Helium

- (id)initWithPath:(NSString *)path error:(NSError **)error {
    self = [super init];
    if (self) {
        _db = tcadbnew();

        if (!tcadbopen(_db, [path UTF8String])) {
            if (error) {
                NSError *e = [self dbError];
                if (!e)
                    e = [NSError errorWithDomain:HeliumDomain code:0 userInfo:@{NSLocalizedDescriptionKey: @"Error opening database"}];
                *error = e;
            }
            tcadbdel(_db);
            _db = nil;
            return nil;
        }

        self.defaultKeyType = HeliumStringValue;
        self.defaultObjectType = HeliumStringValue;
    }
    return self;
}

- (void)dealloc {
    if (_db) {
        [self close];
        tcadbdel(_db);
    }
}

- (void)close {
    tcadbclose(_db);
}

- (BOOL)flush {
    return [self settingErrorDo:^{
        return tcadbsync(_db);
    }];
}

- (TCADB *)db {
    return _db;
}

- (NSError *)dbError {
    if (!_db || _db->omode == ADBOVOID) {
        return [NSError errorWithDomain:HeliumDomain code:0 userInfo:@{NSLocalizedDescriptionKey: @"Database closed"}];
    }
    int ecode = TCESUCCESS;
    if (_db->hdb) {
        ecode = tchdbecode(_db->hdb);
    } else if (_db->bdb) {
        ecode = tcbdbecode(_db->bdb);
    } else if (_db->fdb) {
        ecode = tcfdbecode(_db->fdb);
    } else if (_db->tdb) {
        ecode = tctdbecode(_db->tdb);
    }
    if (ecode != TCESUCCESS) {
        const char *msg = tcerrmsg(ecode);
        return [NSError errorWithDomain:HeliumDomain code:ecode userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithUTF8String:msg]}];
    } else {
        return nil;
    }
}

- (NSString *)dbPath {
    const char *dbPath = tcadbpath(_db);
    if (dbPath != NULL) {
        return [NSString stringWithUTF8String:dbPath];
    } else {
        return nil;
    }
}

- (NSUInteger)count {
    return tcadbrnum(_db);
}

- (BOOL)setObject:(id)value forKey:(id)key;
{
    return [self setObject:value withType:self.defaultObjectType forKey:key keyType:self.defaultKeyType];
}


- (BOOL)setObject:(id)value withType:(HeliumValueType)objectType forKey:(id)key keyType:(HeliumValueType)keyType {
    if (value == nil) {
        return [self removeObjectForKey:key keyType:keyType];
    }
    if (objectType == HeliumStringValue && keyType == HeliumStringValue) {
        const char *utf8Key = [[key description]UTF8String];
        if (!utf8Key) {
            self.lastError = EmptyKeyError;
            return NO;
        }
        const char *utf8Value = [[value description]UTF8String];
        if (!utf8Value) {
            return [self removeObjectForKey:key];
        }
        BOOL res = tcadbput2(_db, utf8Key, utf8Value);
        if (!res) {
            self.lastError = [self dbError];
        }
        return res;
    } else {
        NSData *keyData = [self serializeValue:key type:keyType];
        if (keyData.length == 0) {
            self.lastError = EmptyKeyError;
            return NO;
        }
        NSData *valueData = [self serializeValue:value type:objectType];
        if (valueData.length == 0) {
            self.lastError = EmptyValueError;
            return NO;
        }
        BOOL res = tcadbput(_db, keyData.bytes, keyData.length, valueData.bytes, valueData.length);
        if (!res) {
            self.lastError = [self dbError];
        }
        return res;
    }
}

- (BOOL)removeObjectForKey:(id)key {
    return [self removeObjectForKey:key keyType:self.defaultKeyType];
}

- (BOOL)removeObjectForKey:(id)key keyType:(HeliumValueType)keyType {
    if (keyType == HeliumStringValue) {
        NSString *stringKey = [key description];
        if ([stringKey length] == 0) {
            self.lastError = EmptyKeyError;
            return NO;
        }
        BOOL res = tcadbout2(_db, [stringKey UTF8String]);
        if (!res) {
            self.lastError = [self dbError];
        }
        return res;
    } else {
        NSData *keyData = [self serializeValue:key type:keyType];
        if (keyData.length == 0) {
            self.lastError = EmptyKeyError;
            return NO;
        }
        BOOL res = tcadbout(_db, keyData.bytes, keyData.length);
        if (!res) {
            self.lastError = [self dbError];
        }
        return res;
    }
}

- (BOOL)removeAllObjects {
    return [self settingErrorDo:^{
        return tcadbvanish(_db);
    }];
}

- (id)objectForKey:(id)key {
    if (!key) {
        self.lastError = EmptyKeyError;
        return nil;
    }
    return [self objectWithType:self.defaultObjectType forKey:key keyType:self.defaultKeyType];
}

- (id)objectWithType:(HeliumValueType)objectType forKey:(id)key keyType:(HeliumValueType)keyType {
    if (objectType == HeliumStringValue && keyType == HeliumStringValue) {
        NSString *stringKey = [key description];
        if ([stringKey length] == 0) {
            self.lastError = EmptyKeyError;
            return nil;
        }
        char *cstring = tcadbget2(_db, [stringKey UTF8String]);
        if (cstring) {
            NSString *string = [NSString stringWithUTF8String:cstring];;
            free(cstring);
            return string;
        } else {
            return nil;
        }
    } else {
        NSData *keyData = [self serializeValue:key type:keyType];
        if (keyData.length == 0) {
            self.lastError = EmptyKeyError;
            return nil;
        }
        int bufferSize;
        void *buffer = tcadbget(_db, keyData.bytes, keyData.length, &bufferSize);
        if (buffer == nil) {
            return nil;
        }
        id value = [self unserializeValue:buffer length:bufferSize type:objectType freeWhenDone:YES];
        return value;
    }
}

- (NSEnumerator *)allKeys {
    return [[HeliumKeyEnumerator alloc]initWithDatabase:self type:self.defaultKeyType];
}

- (NSArray *)keysWithPrefix:(id)prefix {
    return [self keysWithPrefix:prefix keyType:self.defaultKeyType limit:-1];
}

- (NSArray *)keysWithPrefix:(id)prefix keyType:(HeliumValueType)keyType limit:(int)limit {
    TCLIST *keys;
    if (keyType == HeliumStringValue) {
        keys = tcadbfwmkeys2(_db, [prefix UTF8String], limit);
    } else {
        NSData *prefixData = [self serializeValue:prefix type:keyType];
        keys = tcadbfwmkeys(_db, prefixData.bytes, prefixData.length, limit);
    }
    return [[HeliumListArray alloc]initWithDatabase:self list:keys type:keyType];
}

- (NSDictionary *)allObjects {
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    for (id key in self.allKeys) {
        result[key] = [self objectForKey:key];
    }
    return result;
}

- (BOOL)optimize {
    return [self settingErrorDo:^{
        return tcadboptimize(_db, NULL);
    }];
}

- (BOOL)settingErrorDo:(bool (^)(void))block {
    if (!block()) {
        self.lastError = self.dbError;
        return NO;
    }
    return YES;
}


- (NSData *)serializeValue:(id)value type:(HeliumValueType)type {
    switch (type) {
    case HeliumStringValue:
    case HeliumNumberValue: {
        NSString *string;
        if (value == [NSNull null]) {
            string = @"";
        } else {
            string = [value description];
        }
        return [string dataUsingEncoding:NSUTF8StringEncoding];
    }
    case HeliumDateValue: {
        NSString *string;
        if ([value isKindOfClass:NSDate.class]) {
            string = [NSString stringWithFormat:@"%f", [value timeIntervalSince1970]];
        } else {
            string = @"";
        }
        return [string dataUsingEncoding:NSUTF8StringEncoding];
    }
    case HeliumUInt64Value: {
        unsigned long long i = [value unsignedLongLongValue];
        return [NSData dataWithBytes:&i length:sizeof(unsigned long long)];
    }
    case HeliumUInt32Value: {
        unsigned long i = [value unsignedLongLongValue];
        return [NSData dataWithBytes:&i length:sizeof(unsigned long)];
    }
    case HeliumPlistValue: {
        NSError *error = nil;
        NSData *data = [NSPropertyListSerialization dataWithPropertyList:value format:NSPropertyListBinaryFormat_v1_0 options:0 error:&error];
        if (error)
            self.lastError = error;
        return data;
    }
    case HeliumDataValue: {
        return value;
    }
    default:
        return nil;
    }
}


- (id)unserializeValue:(void *)value length:(int)length type:(HeliumValueType)type freeWhenDone:(BOOL)freeWhenDone {
    if (!value) {
        return nil;
    }
    switch (type) {
    case HeliumStringValue: {
        NSString *string = [[NSString alloc]initWithBytes:value length:length encoding:NSUTF8StringEncoding];
        if (freeWhenDone)
            free(value);
        return string;
    }
    case HeliumNumberValue: {
        NSString *string = [[NSString alloc]initWithBytesNoCopy:value length:length encoding:NSUTF8StringEncoding freeWhenDone:freeWhenDone];
        if (string.length == 0)
            return nil;
        return @(string.integerValue);
    }
    case HeliumDateValue: {
        NSString *string = [[NSString alloc]initWithBytesNoCopy:value length:length encoding:NSUTF8StringEncoding freeWhenDone:freeWhenDone];
        if (string.length == 0)
            return nil;
        return [NSDate dateWithTimeIntervalSince1970:string.doubleValue];
    }
    case HeliumUInt64Value: {
        NSNumber *number = [NSNumber numberWithUnsignedLongLong:(unsigned long long)&value];
        if (freeWhenDone)
            free(value);
        return number;
    }
    case HeliumUInt32Value: {
        NSNumber *number = [NSNumber numberWithUnsignedLongLong:(unsigned long)&value];
        if (freeWhenDone) {
            free(value);
        }
        return number;
    }
    case HeliumPlistValue: {
        NSData *data = [[NSData alloc]initWithBytesNoCopy:value length:length freeWhenDone:freeWhenDone];
        NSError *error = nil;
        id obj = [NSPropertyListSerialization propertyListWithData:data options:NSPropertyListMutableContainersAndLeaves format:nil error:&error];
        if (error)
            self.lastError = error;
        return obj;
    }
    case HeliumDataValue: {
        NSData *data = [[NSData alloc]initWithBytes:value length:length];
        if (freeWhenDone)
            free(value);
        return data;
    }
    default:
        if (freeWhenDone)
            free(value);
        return nil;
    }
}


@end

@implementation HeliumKeyEnumerator

- (id)initWithDatabase:(Helium *)database type:(HeliumValueType)type {
    self = [super init];
    if (self) {
        _database = database;
        _type = type;
        _init = NO;
    }
    return self;
}

- (id)nextObject {
    if (!_init) {
        if (!tcadbiterinit(_database.db)) {
            _database.lastError = [_database dbError];
            return nil;
        }
        _init = YES;
    }
    if (_type == HeliumStringValue) {
        char *key = tcadbiternext2(_database.db);
        if (key == NULL)
            return nil;
        NSString *stringKey = [NSString stringWithUTF8String:key];
        free(key);
        return stringKey;
    } else {
        int sp;
        void *key = tcadbiternext(_database.db, &sp);
        if (key == NULL)
            return nil;
        id objectKey = [_database unserializeValue:key length:sp type:_type freeWhenDone:YES];
        return objectKey;
    }
}


@end

@interface HeliumListEnumerator : NSEnumerator

@property (nonatomic, assign) NSInteger i;
@property (nonatomic, strong) HeliumListArray *array;

@end

@implementation HeliumListEnumerator

- (id)nextObject {
    return self.array[self.i++];
}

@end

@implementation HeliumListArray

- (id)initWithDatabase:(Helium *)database list:(TCLIST *)list type:(HeliumValueType)type {
    self = [super init];
    if (self) {
        _database = database;
        _list = list;
        _type = type;
    }
    return self;
}

- (void)dealloc {
    tclistdel(_list);
}

- (id)objectAtIndex:(NSUInteger)index {
    if (_type == HeliumStringValue) {
        const char *value = tclistval2(_list, index);
        return value ? [NSString stringWithUTF8String:value] : nil;
    } else {
        int sp;
        const void *data = tclistval(_list, index, &sp);
        return [_database unserializeValue:(void *)data length:sp type:_type freeWhenDone:NO];
    }
}

- (NSUInteger)count {
    return tclistnum(_list);
}

- (NSEnumerator *)objectEnumerator {
    HeliumListEnumerator *e = [HeliumListEnumerator new];
    e.array = self;
    return e;
}

@end

@implementation HeliumTable

- (id)initWithPath:(NSString *)path error:(NSError **)error {
    return [self initWithPath:path options:nil error:error];
}

- (id)initWithPath:(NSString *)path options:(NSString *)options error:(NSError **)error {
    NSMutableString *pathWithOptions = [path mutableCopy];
    if (![pathWithOptions.pathExtension isEqual:@"tct"]) {
        [pathWithOptions appendString:@".tct"];
    }
    if (options) {
        [pathWithOptions appendFormat:@"#%@", options];
    }
    self = [super initWithPath:pathWithOptions error:error];
    if (self) {
        _columnTypes = [NSMutableDictionary new];
        self.defaultKeyType = HeliumNumberValue;
        self.defaultObjectType = HeliumStringValue;
    }
    return self;
}

- (BOOL)configureIndex:(HeliumIndexOperation)indexOperation forColumn:(NSString *)columnName {
    if (![self checkIfTableIsOpen]) return NO;
    if (!tctdbsetindex(_db->tdb, [columnName UTF8String], indexOperation)) {
        self.lastError = [self dbError];
        return NO;
    }
    return YES;
}


- (void)setType:(HeliumValueType)type forColumn:(NSString *)columnName {
    [_columnTypes setObject:[NSNumber numberWithInt:type] forKey:columnName];
}

- (HeliumValueType)typeForColumn:(NSString *)columnName {
    NSNumber *t = [_columnTypes objectForKey:columnName];
    HeliumValueType tt = t ? t.intValue : self.defaultObjectType;
    return tt;
}

- (BOOL)setObject:(id)value withType:(HeliumValueType)valueType forKey:(id)key keyType:(HeliumValueType)keyType {
    if (![value isKindOfClass:[NSDictionary class]]) {
        NSString *errorMessage = [NSString stringWithFormat:@"bad table database value (expected NSDictionary): %@", value];
        self.lastError = [NSError errorWithDomain:HeliumDomain code:0 userInfo:@{NSLocalizedDescriptionKey: errorMessage}];
        return NO;
    }

    NSData *keyData = [self serializeValue:key type:keyType];
    TCMAP *cols = [self createMapFromColumns:value defaultValueType:valueType];
    if (![self checkIfTableIsOpen]) return NO;
    BOOL res = tctdbput(_db->tdb, keyData.bytes, keyData.length, cols);
    tcmapdel(cols);
    if (!res) {
        self.lastError = [self dbError];
    }
    return res;
}

- (TCMAP *)createMapFromColumns:(NSDictionary *)columns defaultValueType:(HeliumValueType)defaultType {
    TCMAP *map = tcmapnew2(columns.count);
    [columns enumerateKeysAndObjectsUsingBlock:^(NSString *name, id value, BOOL *stop) {
        NSNumber *specificType = _columnTypes[name];
        HeliumValueType type = specificType ? (HeliumValueType)specificType.intValue : defaultType;

        NSData *nameData = [self serializeValue:name type:HeliumStringValue];
        NSData *valueData = [self serializeValue:value type:type];
        if (!nameData.length || !valueData.length)
            return;

        tcmapput(map, nameData.bytes, nameData.length, valueData.bytes, valueData.length);
    }];
    return map;
}

- (id)objectWithType:(HeliumValueType)valueType forKey:(id)key keyType:(HeliumValueType)keyType {
    if (!key) {
        self.lastError = [NSError errorWithDomain:HeliumDomain code:0 userInfo:@{NSLocalizedDescriptionKey: @"Empty key"}];
        return nil;
    }

    NSData *keyData = [self serializeValue:key type:keyType];
    if (![self checkIfTableIsOpen]) return nil;
    TCMAP *cols = tctdbget(_db->tdb, keyData.bytes, keyData.length);
    if (cols == NULL) {
        return nil;
    }
    NSUInteger count = tcmaprnum(cols);
    NSMutableDictionary *value = [NSMutableDictionary dictionaryWithCapacity:count];
    tcmapiterinit(cols);
    int columnNameSize;
    const void *columnNameBuffer;
    while ((columnNameBuffer = tcmapiternext(cols, &columnNameSize))) {
        int columnValueSize;
        const void *columnValueBuffer = tcmapget(cols, columnNameBuffer, columnNameSize, &columnValueSize);
        if (columnValueBuffer) {
            NSString *columnName = [[NSString alloc]initWithBytesNoCopy:(void *)columnNameBuffer length:columnNameSize encoding:NSUTF8StringEncoding freeWhenDone:NO];
            NSNumber *t = [_columnTypes objectForKey:columnName];
            HeliumValueType tt = t ? t.intValue : valueType;
            id columnValue = [self unserializeValue:(void *)columnValueBuffer length:columnValueSize type:tt freeWhenDone:NO];
            if (columnValue) {
                [value setObject:columnValue forKey:columnName];
            }
        }
    }
    tcmapdel(cols);
    return value;
}

- (NSNumber *)generateUniqueID {
    if (![self checkIfTableIsOpen]) return nil;
    int64_t uid = tctdbgenuid(_db->tdb);
    if (uid < 0) {
        return nil;
    } else {
        return [NSNumber numberWithLongLong:uid];
    }
}

- (HeliumQuery *)newQuery {
    return [[HeliumQuery alloc]initWithDatabase:self];
}

- (BOOL)checkIfTableIsOpen {
    if (_db->omode != ADBOTDB) {
        self.lastError = [NSError errorWithDomain:HeliumDomain code:TCEMISC + 1 userInfo:@{NSLocalizedDescriptionKey: @"Database is closed"}];
        return NO;
    } else {
        return YES;
    }
}

@end

@implementation HeliumQueryCondition

- (id)initWithColumnName:(NSString *)columnName condition:(int)condition expr:(NSString *)expr {
    self = [super init];
    if (self) {
        self.columnName = columnName;
        self.condition = condition;
        self.expr = expr;
    }
    return self;
}

- (HeliumQueryCondition *)negate {
    self.condition = self.condition | TDBQCNEGATE;
    return self;
}

- (HeliumQueryCondition *)negate:(BOOL)negate {
    if (negate) {
        self.condition |= TDBQCNEGATE;
    } else {
        self.condition &= ~TDBQCNEGATE;
    }
    return self;
}

- (HeliumQueryCondition *)noIndex {
    self.condition = self.condition | TDBQCNOIDX;
    return self;
}

@end

@implementation HeliumQuery

- (id)initWithDatabase:(HeliumTable *)database {
    self = [super init];
    if (self) {
        _database = database;
        _conditions = [NSMutableArray new];
    }
    return self;
}

- (HeliumQueryCondition *)column:(NSString *)columnName equals:(id)stringOrNumber {
    HeliumQueryCondition *c = [[HeliumQueryCondition alloc]initWithColumnName:columnName
      condition:[_database typeForColumn:columnName] == HeliumStringValue ? TDBQCSTREQ : TDBQCNUMEQ
      expr:[stringOrNumber description]];
    [_conditions addObject:c];
    return c;
}

- (HeliumQueryCondition *)column:(NSString *)columnName includes:(NSString *)string {
    HeliumQueryCondition *c = [[HeliumQueryCondition alloc]initWithColumnName:columnName
      condition:TDBQCSTRINC
      expr:string];
    [_conditions addObject:c];
    return c;
}

- (HeliumQueryCondition *)column:(NSString *)columnName hasPrefix:(NSString *)string {
    HeliumQueryCondition *c = [[HeliumQueryCondition alloc]initWithColumnName:columnName
      condition:TDBQCSTRBW
      expr:string];
    [_conditions addObject:c];
    return c;
}

- (HeliumQueryCondition *)column:(NSString *)columnName hasSuffix:(NSString *)string {
    HeliumQueryCondition *c = [[HeliumQueryCondition alloc]initWithColumnName:columnName
      condition:TDBQCSTREW
      expr:string];
    [_conditions addObject:c];
    return c;
}

- (HeliumQueryCondition *)column:(NSString *)columnName includesAllTokens:(NSString *)string {
    HeliumQueryCondition *c = [[HeliumQueryCondition alloc]initWithColumnName:columnName
      condition:TDBQCSTRAND
      expr:string];
    [_conditions addObject:c];
    return c;
}

- (HeliumQueryCondition *)column:(NSString *)columnName includesAnyToken:(NSString *)string {
    HeliumQueryCondition *c = [[HeliumQueryCondition alloc]initWithColumnName:columnName
      condition:TDBQCSTROR
      expr:string];
    [_conditions addObject:c];
    return c;
}

- (HeliumQueryCondition *)column:(NSString *)columnName equalsAny:(NSArray *)stringsOrNumbers {
    HeliumQueryCondition *c = [[HeliumQueryCondition alloc]initWithColumnName:columnName
      condition:[_database typeForColumn:columnName] == HeliumStringValue ? TDBQCSTROREQ : TDBQCNUMOREQ
      expr:[stringsOrNumbers componentsJoinedByString:@" "]];
    [_conditions addObject:c];
    return c;
}

- (HeliumQueryCondition *)column:(NSString *)columnName matchesRegex:(NSString *)regex {
    HeliumQueryCondition *c = [[HeliumQueryCondition alloc]initWithColumnName:columnName
      condition:TDBQCSTRRX
      expr:regex];
    [_conditions addObject:c];
    return c;
}

- (HeliumQueryCondition *)column:(NSString *)columnName greaterThan:(NSNumber *)number {
    HeliumQueryCondition *c = [[HeliumQueryCondition alloc]initWithColumnName:columnName
      condition:TDBQCNUMGT
      expr:[number stringValue]];
    [_conditions addObject:c];
    return c;
}

- (HeliumQueryCondition *)column:(NSString *)columnName greaterThanOrEquals:(NSNumber *)number {
    HeliumQueryCondition *c = [[HeliumQueryCondition alloc]initWithColumnName:columnName
      condition:TDBQCNUMGE
      expr:[number stringValue]];
    [_conditions addObject:c];
    return c;
}

- (HeliumQueryCondition *)column:(NSString *)columnName lessThan:(NSNumber *)number {
    HeliumQueryCondition *c = [[HeliumQueryCondition alloc]initWithColumnName:columnName
      condition:TDBQCNUMLT
      expr:[number stringValue]];
    [_conditions addObject:c];
    return c;
}

- (HeliumQueryCondition *)column:(NSString *)columnName lessThanOrEquals:(NSNumber *)number {
    HeliumQueryCondition *c = [[HeliumQueryCondition alloc]initWithColumnName:columnName
      condition:TDBQCNUMLE
      expr:[number stringValue]];
    [_conditions addObject:c];
    return c;
}

- (HeliumQueryCondition *)column:(NSString *)columnName between:(NSNumber *)number other:(NSNumber *)otherNumber {
    HeliumQueryCondition *c = [[HeliumQueryCondition alloc]initWithColumnName:columnName
      condition:TDBQCNUMBT
      expr:[NSString stringWithFormat:@"%@ %@", number, otherNumber]];
    [_conditions addObject:c];
    return c;
}

- (HeliumQueryCondition *)column:(NSString *)columnName searchPhrase:(NSString *)string {
    HeliumQueryCondition *c = [[HeliumQueryCondition alloc]initWithColumnName:columnName
      condition:TDBQCFTSPH
      expr:string];
    [_conditions addObject:c];
    return c;
}

- (HeliumQueryCondition *)column:(NSString *)columnName searchAllTokens:(NSString *)string {
    HeliumQueryCondition *c = [[HeliumQueryCondition alloc]initWithColumnName:columnName
      condition:TDBQCFTSAND
      expr:string];
    [_conditions addObject:c];
    return c;
}

- (HeliumQueryCondition *)column:(NSString *)columnName searchAnyToken:(NSString *)string {
    HeliumQueryCondition *c = [[HeliumQueryCondition alloc]initWithColumnName:columnName
      condition:TDBQCFTSOR
      expr:string];
    [_conditions addObject:c];
    return c;
}

- (HeliumQueryCondition *)column:(NSString *)columnName searchCompoundExpression:(NSString *)string {
    HeliumQueryCondition *c = [[HeliumQueryCondition alloc]initWithColumnName:columnName
      condition:TDBQCFTSEX
      expr:string];
    [_conditions addObject:c];
    return c;
}

- (void)order:(HeliumQueryOrder)order byColumn:(NSString *)columnName {
    _orderByColumnName = columnName;
    if ([_database typeForColumn:columnName] == HeliumStringValue) {
        _orderType = order == HeliumQueryAscendingOrder ? TDBQOSTRASC : TDBQOSTRDESC;
    } else {
        _orderType = order == HeliumQueryAscendingOrder ? TDBQONUMASC : TDBQONUMDESC;
    }
}

- (NSArray *)allKeys {
    return [self keysWithLimit:-1 skip:0];
}

- (id)firstKey {
    NSArray *keys = [self keysWithLimit:1 skip:0];
    if (keys.count == 0)
        return nil;
    return [keys objectAtIndex:0];
}

- (NSArray *)keysWithLimit:(int)limit skip:(int)skip {
    if (!_database.db->tdb) return @[];

    TDBQRY *query = tctdbqrynew(_database.db->tdb);

    for (HeliumQueryCondition *c in _conditions) {
        const char *utf8Name = [c.columnName UTF8String];
        const char *utf8Expr = [c.expr UTF8String];
        if (utf8Name && utf8Expr)
            tctdbqryaddcond(query, utf8Name, c.condition, utf8Expr);
    }

    if (_orderByColumnName) {
        const char *utf8Name = [_orderByColumnName UTF8String];
        if (utf8Name)
            tctdbqrysetorder(query, utf8Name, _orderType);
    }

    if (limit > 0 || skip > 0)
        tctdbqrysetlimit(query, limit, skip);

    TCLIST *keys = tctdbqrysearch(query);
    tctdbqrydel(query);

    return [[HeliumListArray alloc]initWithDatabase:_database list:keys type:_database.defaultKeyType];
}

@end
