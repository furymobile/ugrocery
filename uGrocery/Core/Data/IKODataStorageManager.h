//
//  IKODataStorageManager.h
//  uGrocery
//
//  Created by Duane Schleen on 7/2/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IKODataStore.h"

@interface IKODataStorageManager : NSObject

+ (IKODataStorageManager *)sharedInstance;

- (id)storeForClass:(Class)class;

- (void)addService:(id <IKODataStoreProtocol>)service;
- (void)removeServiceForClass:(Class)class;

- (void)flushDatabases;
- (void)closeDatabases;

@end
