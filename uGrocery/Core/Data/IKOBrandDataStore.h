//
//  IKOBrandDataStore.h
//  uGrocery
//
//  Created by Duane Schleen on 7/26/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKODataStore.h"
#import "IKOBrand.h"

extern NSString *const brandDbName;

@interface IKOBrandDataStore : IKODataStore

- (NSArray *)allObjects;
- (IKOBrand *)getBrand:(NSNumber *)brandId;
- (IKOBrand *)brandFromTerm:(NSString *)term;
- (void)seedBrands:(IKOSeedType)seedType completion:(void (^)())completionBlock;

@end
