//
//  IKORecentSearchDataStore.h
//  uGrocery
//
//  Created by Duane Schleen on 1/15/15.
//  Copyright (c) 2015 ikonomo. All rights reserved.
//

#import "IKODataStore.h"
#import "IKORecentSearchItem.h"

extern NSString *const recentSearchDbName;

@interface IKORecentSearchDataStore : IKODataStore

- (BOOL)saveRecentSearchItem:(IKORecentSearchItem *)recentSearchItem;
- (BOOL)updateTimestamp:(IKORecentSearchItem *)recentSearchItem;
- (NSArray *)allObjects;

@end
