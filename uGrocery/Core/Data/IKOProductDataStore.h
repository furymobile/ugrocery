//
//  IKOProductDataStore.h
//  uGrocery
//
//  Created by Duane Schleen on 7/6/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKODataStore.h"
#import "IKOProduct.h"
#import "IKOBrand.h"
#import "IKOProductCategory.h"

@class IKOError;
@class IKOServerResponse;
@class IKOPagingInfo;

extern NSString *const productDbName;

@interface IKOProductDataStore : IKODataStore

- (BOOL)saveProduct:(IKOProduct *)product;

- (void)getProductForUPC:(NSString *)upcCode callback:(void (^)(IKOError *error, IKOProduct *product, IKOServerResponse *response))completion;

- (IKOProduct *)getProduct:(NSNumber *)productId;
- (void)searchProducts:(NSString *)term pagingInfo:(IKOPagingInfo *)pagingInfo callback:(void (^)(IKOError *error, NSArray *products, IKOServerResponse *response))completion;
- (void)allProductsWithPagingInfo:(IKOPagingInfo *)pagingInfo callback:(void (^)(IKOError *error, NSArray *products, IKOServerResponse *response))completion;
- (void)productsForL1:(NSNumber *)categoryId pagingInfo:(IKOPagingInfo *)pagingInfo callback:(void (^)(IKOError *error, NSArray *products, IKOServerResponse *response))completion;
- (void)productsForCategory:(NSNumber *)categoryId pagingInfo:(IKOPagingInfo *)pagingInfo callback:(void (^)(IKOError *error, NSArray *products, IKOServerResponse *response))completion;
- (void)productsForBrand:(NSNumber *)brandId pagingInfo:(IKOPagingInfo *)pagingInfo callback:(void (^)(IKOError *error, NSArray *products, IKOServerResponse *response))completion;
- (NSArray *)allObjects;

- (void)seedProducts:(void (^)())completionBlock;

@end
