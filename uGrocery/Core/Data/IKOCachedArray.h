#import <Foundation/Foundation.h>

@interface IKOCachedArray : NSMutableArray

@property (nonatomic) id context;

- (id)initWithLoadObject:(id (^)(NSUInteger))loadObject
  getCount:(NSUInteger (^)())getCount
  cacheSize:(NSUInteger)cacheSize
  context:(id)context;

@end
