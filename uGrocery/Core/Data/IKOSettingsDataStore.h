//
//  IKOSettingsDataStore.h
//  uGrocery
//
//  Created by Duane Schleen on 7/2/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKODataStore.h"

extern NSString *const kLastDeals;
extern NSString *const kLastDealsStore;
extern NSString *const kLastDealsUpdatedOn;
extern NSString *const kLastPriceCaptureExpanded;
extern NSString *const kLastTabIndex;
extern NSString *const kLastUsedBrandKey;
extern NSString *const kLastUsedCategoryKey;
extern NSString *const kLastUsedPrice;
extern NSString *const kLastUsedSaleDescription;
extern NSString *const kLastUsedSaleEndDate;
extern NSString *const kLastUsedSalePrice;
extern NSString *const kLastUsedStoreKey;
extern NSString *const kLastUsedUpdatedOn;
extern NSString *const kShowAdvancedPriceCapture;
extern NSString *const kShowCoupons;
extern NSString *const kShowStoreBrands;

@interface IKOSettingsDataStore : IKODataStore

- (BOOL)hasSettingForKey:(NSString *)key;
- (BOOL)setString:(NSString *)string forKey:(NSString *)key;
- (BOOL)setObject:(id<NSCoding>)object forKey:(NSString *)key;
- (BOOL)setImage:(UIImage *)image forKey:(NSString *)key;
- (id)objectForKey:(NSString *)key;
- (NSString *)stringForKey:(NSString *)key;
- (UIImage *)imageForKey:(NSString *)key;
- (BOOL)removeObjectForKey:(NSString *)key;

@end
