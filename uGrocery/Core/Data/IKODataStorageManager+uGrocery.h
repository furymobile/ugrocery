//
//  FCStorageManager+CardReader.h
//  card-reader-ios
//
//  Created by Duane Schleen on 3/10/14.
//  Copyright (c) 2014 FullContact. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IKODataStorageManager.h"

#import "IKOBrandDataStore.h"
#import "IKOFavoritesDataStore.h"
#import "IKOPriceUpdateDataStore.h"
#import "IKOProductCategoryDataStore.h"
#import "IKOProductDataStore.h"
#import "IKORecentSearchDataStore.h"
#import "IKOSearchDataStore.h"
#import "IKOSettingsDataStore.h"
#import "IKOShoppingListDataStore.h"
#import "IKOStoreDataStore.h"

@interface IKODataStorageManager (uGrocery)

+ (void)initialize:(void (^)())completionBlock;

@end
