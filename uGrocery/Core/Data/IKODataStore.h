//
//  IKODataStore.h
//  uGrocery
//
//  Created by Duane Schleen on 7/2/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Helium.h"

typedef NS_ENUM(NSInteger, IKOSeedType) {
    IKOSeedTypePublic = 0,
    IKOSeedTypeAdmin,
    IKOSeedTypeIncremental
};

@protocol IKODataStoreProtocol <NSObject>

@required
- (Class)targetClass;

@optional
- (BOOL)openDatabase;
- (void)closeDatabase;
- (void)flush;

@end

@interface IKODataStore : NSObject <IKODataStoreProtocol>

- (BOOL)addSkipBackupAttributeToItemAtPath:(NSString *)URL;
- (void)seedDatabaseNamed:(NSString *)name;
- (NSString *)nameForSeedType:(IKOSeedType)seedType;

- (NSString *)lastUpdatedTimestamp;
- (void)updateTimestamp:(NSDate *)date;
- (void)updateTimestamp;
        
@end
