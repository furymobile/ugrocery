//
//  IKODataStore.h
//  uGrocery
//
//  Created by Duane Schleen on 7/2/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKODataStore.h"
#import "PDKeychainBindings.h"

@implementation IKODataStore

- (Class)targetClass {
    @throw [NSException
           exceptionWithName:@"Abstract method not implemented"
           reason:[NSString stringWithFormat:@"Expected subclass %@ to define method %@",
           NSStringFromClass([self class]),
           NSStringFromSelector(_cmd)]
           userInfo:nil];
}

- (void)dealloc {
    if ([self respondsToSelector:@selector(closeDatabase)])
        [self closeDatabase];
}

- (void)seedDatabaseNamed:(NSString *)name {
#ifdef SEED
    NSLog(@"SEEDING %@ ABORTED - SEED DEFINED", [name uppercaseString]);
    return;
#endif
    
    NSString *fileName = [name stringByDeletingPathExtension];
    NSString *extension = [name pathExtension];
    
    NSString *seedDbPath = [[NSBundle mainBundle]pathForResource:fileName ofType:extension];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)lastObject];
    NSString *destinationPath = [documentsDirectory stringByAppendingPathComponent:name];
    
    NSString *post13 = [[PDKeychainBindings sharedKeychainBindings] objectForKey:@"POST13"];
    
    //The following if statement fixes an issue where there were duplicate brands and categories in 1.3
    if (!post13) {
        if ([name isEqualToString:@"ugbdb.tct"] || [name isEqualToString:@"ugpcdb.tct"] || [name isEqualToString:@"ugschdb.tct"]) {
            
            NSString *f1 = [documentsDirectory stringByAppendingPathComponent:@"ugbdb.tct"];
            NSString *f2 = [documentsDirectory stringByAppendingPathComponent:@"ugpcdb.tct"];
            NSString *f3 = [documentsDirectory stringByAppendingPathComponent:@"ugschdb.tct"];
            
            [fileManager removeItemAtPath:f1 error:&error];
            [fileManager removeItemAtPath:f2 error:&error];
            [fileManager removeItemAtPath:f3 error:&error];
            
            [[PDKeychainBindings sharedKeychainBindings] setObject:@"POST13" forKey:@"POST13"];
        }
    }
    
    if ([fileManager fileExistsAtPath:destinationPath] == NO) {
        NSLog(@"SEEDING DATABASE: %@", name);
        NSLog(@"Seed db Path: %@", seedDbPath);
        NSLog(@"Destination db Path: %@", destinationPath);
        [fileManager copyItemAtPath:seedDbPath toPath:destinationPath error:&error];
    }
    
    if (error)
        NSLog(@"Error seeding db: %@.  Error: %@", name, error.localizedDescription);
}

- (BOOL)addSkipBackupAttributeToItemAtPath:(NSString *)path {
    NSURL *URL = [NSURL fileURLWithPath:path];
    if (![[NSFileManager defaultManager]fileExistsAtPath:[URL path]])
        return NO;

    NSError *error = nil;
    BOOL success = [URL setResourceValue:[NSNumber numberWithBool:YES]
      forKey:NSURLIsExcludedFromBackupKey error:&error];
    if(!success) {
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    } else {
        NSLog(@"Successfully excluded %@ from backup", [URL lastPathComponent]);
    }
    return success;
}

- (NSString *)nameForSeedType:(IKOSeedType)seedType {
    switch (seedType) {
        case IKOSeedTypePublic:
            return @"public";
            break;
        case IKOSeedTypeAdmin:
            return @"admin";
            break;
        case IKOSeedTypeIncremental:
            return @"public";
            break;
        default:
            return nil;
    }
}

- (NSString *) lastUpdatedTimestamp {
    NSDate *date = [[NSUserDefaults standardUserDefaults] objectForKey:@"Last Updated Timestamp"];
    if (!date)
        return nil;
    
    return SF(@"%.0f", [date timeIntervalSince1970]);
}

- (void)updateTimestamp:(NSDate *)date {
    NSLog(@"Seed stamp set to: %f", [date timeIntervalSince1970]);
    [[NSUserDefaults standardUserDefaults] setObject:date forKey:@"Last Updated Timestamp"];
}

- (void)updateTimestamp {
    [self updateTimestamp:[NSDate date]];
}

@end
