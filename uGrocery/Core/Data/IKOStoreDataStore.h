//
//  IKOStoreDataStore.h
//  uGrocery
//
//  Created by Duane Schleen on 10/30/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKODataStore.h"
#import "IKOStore.h"

@interface IKOStoreDataStore : IKODataStore

+ (NSArray *)allStoreIds;

- (NSArray *)allObjects;
- (UIImage *)storeImageFor:(NSNumber *)storeId;
- (NSString *)storeNameFor:(NSNumber *)storeId;
- (UIImage *)largerStoreImageFor:(NSNumber *)storeId;

@end
