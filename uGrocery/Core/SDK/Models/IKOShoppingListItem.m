//
//  IKOShoppingListItem.m
//  uGrocery
//
//  Created by Duane Schleen on 7/15/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOShoppingListItem.h"
#import "IKOProduct.h"
#import "IKOSearchItem.h"
#import "IKOProductCategory.h"
#import "IKOFavoriteItem.h"

@implementation IKOShoppingListItem

- (instancetype)initWithProduct:(IKOProduct *)product {
    self = [super init];
    if (self) {
        self.shoppingListItemId = [[NSUUID UUID]UUIDString];
        self.productId = product.productId;
        self.categoryId = product.categoryId ? : @0;
        self.brandId = product.brandId ? : @0;
        self.listItemDescription = product.productDescription;
        if ([product.hasImage integerValue] == 1)
            self.imageUri = product.imageUri;
        self.quantity = @1;
        self.crossedOff = @0;
        self.comparable = product.comparable;
        self.measureText = product.measureText;
        self.isAdhoc = @0;
    }
    return self;
}

- (instancetype)initWithProductCategory:(IKOProductCategory *)productCategory {
    self = [super init];
    if (self) {
        self.shoppingListItemId = [[NSUUID UUID]UUIDString];
        self.categoryId = productCategory.productCategoryId;
        self.brandId = @0;
        self.productId = @0;
        self.listItemDescription = productCategory.name;
        self.quantity = @1;
        self.crossedOff = @0;
        self.comparable = @0;
        self.measureText = nil;
        self.isAdhoc = @0;
    }
    return self;
}

- (instancetype)initWithSearchItem:(IKOSearchItem *)searchItem {
    self = [super init];
    if (self) {
        self.shoppingListItemId = [[NSUUID UUID]UUIDString];
        self.categoryId = searchItem.categoryId ? : @0;
        self.brandId = searchItem.brandId ? : @0;
        self.productId = @0;
        self.listItemDescription = searchItem.itemDescription;
        self.quantity = @1;
        self.crossedOff = @0;
        self.comparable = @0;
        self.measureText = searchItem.measureText;
        self.isAdhoc = @0;
    }
    return self;
}

- (instancetype)initWithMyProductItem:(IKOFavoriteItem *)myProductItem {
    self = [super init];
    if (self) {
        self.shoppingListItemId = [[NSUUID UUID]UUIDString];
        self.categoryId = myProductItem.categoryId ? : @0;
        self.brandId = myProductItem.brandId ? : 0;
        self.productId = myProductItem.productId ? : @0;
        self.listItemDescription = myProductItem.productDescription;
        self.imageUri = myProductItem.imageUri;
        self.quantity = @1;
        self.crossedOff = @0;
        self.comparable = myProductItem.comparable;
        self.measureText = myProductItem.measureText;
        self.isAdhoc = @0;
    }
    return self;
}

+ (NSArray *)maps {
    return @[
        [FMap mapPropertyName:@"shoppingListItemId" toJsonKey:@"Id"],
        [FMap mapPropertyName:@"userId" toJsonKey:@"UserId"],
        [FMap mapPropertyName:@"productId" toJsonKey:@"ProductId"],
        [FMap mapPropertyName:@"categoryId" toJsonKey:@"CategoryId"],
        [FMap mapPropertyName:@"brandId" toJsonKey:@"BrandId"],
        [FMap mapPropertyName:@"l1Id" toJsonKey:@"L1Id"],
        [FMap mapPropertyName:@"listItemDescription" toJsonKey:@"ListItemDescription"],
        [FMap mapPropertyName:@"imageUri" toJsonKey:@"ImageUri"],
        [FMap mapPropertyName:@"quantity" toJsonKey:@"Quantity"],
        [FMap mapPropertyName:@"crossedOff" toJsonKey:@"CrossedOff"],
        [FMap mapPropertyName:@"comparable" toJsonKey:@"Comparable"],
        [FMap mapPropertyName:@"measureText" toJsonKey:@"MeasureText"],
        [FMap mapPropertyName:@"isAdhoc" toJsonKey:@"IsAdhoc"]
    ];
}

@end
