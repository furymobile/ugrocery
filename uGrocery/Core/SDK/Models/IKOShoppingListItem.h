//
//  IKOShoppingListItem.h
//  uGrocery
//
//  Created by Duane Schleen on 7/15/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOObject.h"

@class IKOProduct;
@class IKOProductCategory;
@class IKOSearchItem;
@class IKOFavoriteItem;

@interface IKOShoppingListItem : IKOObject

@property (nonatomic) NSString *shoppingListItemId;
@property (nonatomic) NSNumber *userId;
@property (nonatomic) NSNumber *productId;
@property (nonatomic) NSNumber *categoryId;
@property (nonatomic) NSNumber *brandId;
@property (nonatomic) NSNumber *l1Id;
@property (nonatomic) NSString *listItemDescription;  //Could be category name or product description
@property (nonatomic) NSString *imageUri;
@property (nonatomic) NSNumber *quantity;
@property (nonatomic) NSNumber *crossedOff;
@property (nonatomic) NSNumber *comparable;
@property (nonatomic) NSString *measureText;
@property (nonatomic) NSNumber *isAdhoc;

- (instancetype)initWithProduct:(IKOProduct *)product;
- (instancetype)initWithProductCategory:(IKOProductCategory *)productCategory;
- (instancetype)initWithSearchItem:(IKOSearchItem *)searchItem;
- (instancetype)initWithMyProductItem:(IKOFavoriteItem *)myProductItem;

@end
