//
//  IKOServerResponse.m
//  uGrocery
//
//  Created by Duane Schleen on 8/18/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOServerResponse.h"

@implementation IKOServerResponse

+ (NSArray *)maps {
    return @[
        [FMap mapPropertyName:@"type" toJsonKey:@"Type"],
        [FMap mapPropertyName:@"success" toJsonKey:@"Success"],
        [FMap mapPropertyName:@"payload" toJsonKey:@"Payload"],
        [FMap mapPropertyName:@"limit" toJsonKey:@"Limit"],
        [FMap mapPropertyName:@"page" toJsonKey:@"Page"],
        [FMap mapPropertyName:@"total" toJsonKey:@"Total"]
    ];
}

- (IKOPagingInfo *)getPagingInfo {
    return[[IKOPagingInfo alloc] initWithPage:_page andLimit:_limit andTotal:_total];
}

@end
