//
//  IKOProductCategory.h
//  uGrocery
//
//  Created by Duane Schleen on 7/12/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOObject.h"

@interface IKOProductCategory : IKOObject

@property (nonatomic) NSNumber *productCategoryId;
@property (nonatomic) NSNumber *level;
@property (nonatomic) NSString *name;
@property (nonatomic) NSNumber *sort;

@end
