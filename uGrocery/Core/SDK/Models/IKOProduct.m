//
//  IKOProduct.m
//  uGrocery
//
//  Created by Duane Schleen on 7/6/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOProduct.h"

@implementation IKOProduct

+ (NSArray *)maps {
    return @[
        [FMap mapPropertyName:@"productId" toJsonKey:@"Id"],
        [FMap mapPropertyName:@"upc" toJsonKey:@"Upc"],
        [FMap mapPropertyName:@"upce" toJsonKey:@"UpcE"],
        [FMap mapPropertyName:@"categoryId" toJsonKey:@"CategoryId"],
        [FMap mapPropertyName:@"brandId" toJsonKey:@"BrandId"],
        [FMap mapPropertyName:@"productDescription" toJsonKey:@"CoreDescription"],
        [FMap mapPropertyName:@"imageUri" toJsonKey:@"ImageUri"],
        [FMap mapPropertyName:@"hasImage" toJsonKey:@"HaveImage"],
        [FMap mapPropertyName:@"isStoreBrand" toJsonKey:@"StoreBrand"],
        [FMap mapPropertyName:@"hasPrices" toJsonKey:@"HasPrices"],
        [FMap mapPropertyName:@"searchName" toJsonKey:@"SearchName"],
        [FMap mapPropertyName:@"measureText" toJsonKey:@"MeasureText"],
        [FMap mapPropertyName:@"comparable" toJsonKey:@"Comparable"],
        [FMap mapPropertyName:@"status" toJsonKey:@"Status"]
    ];
}

@end
