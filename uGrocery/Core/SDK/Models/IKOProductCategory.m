//
//  IKOProductCategory.m
//  uGrocery
//
//  Created by Duane Schleen on 7/12/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOProductCategory.h"

@implementation IKOProductCategory

+ (NSArray *)maps {
    return @[
        [FMap mapPropertyName:@"productCategoryId" toJsonKey:@"Id"],
        [FMap mapPropertyName:@"level" toJsonKey:@"Level"],
        [FMap mapPropertyName:@"name" toJsonKey:@"Name"],
        [FMap mapPropertyName:@"sort" toJsonKey:@"SortValue"],
    ];
}

@end
