//
//  IKOError.m
//  uGrocery
//
//  Created by Duane Schleen on 8/18/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOError.h"
#import "IKOServerResponse.h"

@implementation IKOError

+ (IKOError *)errorFromResponse:(IKOServerResponse *)response andError:(NSError *)error {
    IKOError *e = [IKOError new];
    e.code = [NSNumber numberWithLong:error.code];
    e.errorDescription = error.localizedDescription;
    e.error = error;

    if (!response.success && [response.payload isKindOfClass:[NSArray class]]) {
        e.responseErrorDescription = ((NSArray *)response.payload)[0];
    }

    return e;
}

+ (NSArray *)maps {
    return @[
        [FMap mapPropertyName:@"code" toJsonKey:@"code"],
        [FMap mapPropertyName:@"errorDescription" toJsonKey:@"error_description"],
        [FMap mapPropertyName:@"responseErrorDescription" toJsonKey:@"response_error_description"],
    ];
}

- (id)initWithCode:(NSNumber*)code {
    self = [super init];
    if (self) {
        self.code = code;
    }
    return self;
}

@end
