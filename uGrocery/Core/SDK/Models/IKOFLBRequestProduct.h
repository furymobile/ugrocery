//
//  IKOFLBRequestProduct.h
//  uGrocery
//
//  Created by Duane Schleen on 8/31/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOObject.h"

@class IKOShoppingListItem;
@class IKOProduct;
@class IKOFavoriteItem;

@interface IKOFLBRequestProduct : IKOObject

@property (nonatomic) NSNumber *productId;
@property (nonatomic) NSString *upc;
@property (nonatomic) NSNumber *quantity;

- (instancetype)initWithShoppingListItem:(IKOShoppingListItem *)shoppingListItem;
- (instancetype)initWithProduct:(IKOProduct *)product;
- (instancetype)initWithUpc:(NSString *)upc;
- (instancetype)initWithMyProductItem:(IKOFavoriteItem *)myProductItem;

@end
