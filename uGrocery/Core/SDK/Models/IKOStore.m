//
//  IKOStore.m
//  uGrocery
//
//  Created by Duane Schleen on 10/30/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOStore.h"

@implementation IKOStore

- (instancetype)initWithId:(NSNumber *)storeId andName:(NSString *)name {
    self = [super init];
    if (self) {
        self.storeId = storeId;
        self.name = name;
    }
    return self;
}

+ (NSArray *)maps {
    return @[
        [FMap mapPropertyName:@"storeId" toJsonKey:@"Id"],
        [FMap mapPropertyName:@"name" toJsonKey:@"Name"],
    ];
}

@end
