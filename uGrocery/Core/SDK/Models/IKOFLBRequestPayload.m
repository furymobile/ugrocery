//
//  IKOFLBRequestPayload.m
//  uGrocery
//
//  Created by Duane Schleen on 8/31/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOFLBRequestPayload.h"
#import "IKOFLBRequestProduct.h"
#import "IKOShoppingListItem.h"

@implementation IKOFLBRequestPayload

- (id)init {
    self = [super init];
    if (self) {
        _withCoupons = @0;
    }
    return self;
}

- (instancetype)initWithShoppingListItems:(NSArray *)shoppingListItems {
    self = [self init];
    if (self) {
        NSMutableArray __block *tempProducts = [NSMutableArray new];
        [shoppingListItems enumerateObjectsUsingBlock:^(IKOShoppingListItem *obj, NSUInteger idx, BOOL *stop) {
            if ([(NSNumber *)obj.crossedOff isEqualToNumber:@0] && [obj.productId integerValue] != 0)
                [tempProducts addObject:[[IKOFLBRequestProduct alloc]initWithShoppingListItem:obj]];
        }];
        _products = tempProducts;
    }
    return self;
}

- (instancetype)initWithProducts:(NSArray *)products {
    self = [self init];
    if (self) {
        NSMutableArray __block *tempProducts = [NSMutableArray new];
        [products enumerateObjectsUsingBlock:^(IKOProduct *obj, NSUInteger idx, BOOL *stop) {
            [tempProducts addObject:[[IKOFLBRequestProduct alloc]initWithProduct:obj]];
        }];
        _products = tempProducts;
    }
    return self;
}

- (instancetype)initWithMyProducts:(NSArray *)myProducts {
    self = [self init];
    if (self) {
        NSMutableArray __block *tempProducts = [NSMutableArray new];
        [myProducts enumerateObjectsUsingBlock:^(IKOFavoriteItem *obj, NSUInteger idx, BOOL *stop) {
            [tempProducts addObject:[[IKOFLBRequestProduct alloc]initWithMyProductItem:obj]];
        }];
        _products = tempProducts;
    }
    return self;
}

- (instancetype)initWithUpc:(NSString *)upc {
    self = [self init];
    if (self) {
        NSMutableArray __block *tempProducts = [NSMutableArray new];
        [tempProducts addObject:[[IKOFLBRequestProduct alloc]initWithUpc:upc]];
        _products = tempProducts;
    }
    return self;
}

+ (NSArray *)maps {
    return @[
        [[FMap mapPropertyName:@"products" toJsonKey:@"Products"]collectionClass:[IKOFLBRequestProduct class]],
        [FMap mapPropertyName:@"withCoupons" toJsonKey:@"WithCoupons"],
        [FMap mapPropertyName:@"flbPath" toJsonKey:@"FlbPath"]
    ];
}

@end
