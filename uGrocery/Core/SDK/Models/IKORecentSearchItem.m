//
//  IKORecentSearchItem.m
//  uGrocery
//
//  Created by Duane Schleen on 1/15/15.
//  Copyright (c) 2015 ikonomo. All rights reserved.
//

#import "IKORecentSearchItem.h"
#import "IKOBrand.h"
#import "IKOProductCategory.h"
#import "IKOSearchItem.h"

@implementation IKORecentSearchItem

- (instancetype)initWithBrand:(IKOBrand *)brand {
    self = [super init];
    if (self) {
        self.recentSearchItemId = [[NSUUID UUID]UUIDString];
        self.brandId = brand.brandId;
        self.categoryId = @0;
        self.termDescription = brand.name;
        self.lastSearchDateLong = [NSNumber numberWithDouble:[NSDate timeIntervalSinceReferenceDate]];
    }
    return self;
}

- (instancetype)initWithProductCategory:(IKOProductCategory *)productCategory {
    self = [super init];
    if (self) {
        self.recentSearchItemId = [[NSUUID UUID]UUIDString];
        self.categoryId = productCategory.productCategoryId;
        self.brandId = @0;
        self.termDescription = productCategory.name;
        self.lastSearchDateLong = [NSNumber numberWithDouble:[NSDate timeIntervalSinceReferenceDate]];
    }
    return self;
}

- (instancetype)initWithSearchItem:(IKOSearchItem *)searchItem {
    self = [super init];
    if (self) {
        self.recentSearchItemId = [[NSUUID UUID]UUIDString];
        self.categoryId = searchItem.categoryId ? : @0;
        self.brandId = searchItem.brandId ? : @0;
        self.termDescription = searchItem.itemDescription;
        self.lastSearchDateLong = [NSNumber numberWithDouble:[NSDate timeIntervalSinceReferenceDate]];
    }
    return self;
}

+ (NSArray *)maps {
    return @[
        [FMap mapPropertyName:@"recentSearchItemId" toJsonKey:@"Id"],
        [FMap mapPropertyName:@"brandId" toJsonKey:@"BrandId"],
        [FMap mapPropertyName:@"categoryId" toJsonKey:@"CategoryId"],
        [FMap mapPropertyName:@"termDescription" toJsonKey:@"TermDescription"],
        [FMap mapPropertyName:@"lastSearchDateLong" toJsonKey:@"LastSearchDateLong"],
    ];
}

@end
