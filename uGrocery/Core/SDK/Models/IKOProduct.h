//
//  IKOProduct.h
//  uGrocery
//
//  Created by Duane Schleen on 7/6/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

typedef NS_ENUM (NSInteger, IKOProductStatus) {
    IKOProductStatusActive = 0,
    IKOProductStatusInactive = 1,
    IKOProductStatusDuplicate = 2,
    IKOProductStatusHold = 3,
    IKOProductStatusImportReview = 4,
    IKOProductStatusHighDelta = 5
};

#import "IKOObject.h"

@interface IKOProduct : IKOObject

@property (nonatomic) NSNumber *productId;
@property (nonatomic) NSString *upc;
@property (nonatomic) NSString *upce;
@property (nonatomic) NSNumber *categoryId;
@property (nonatomic) NSNumber *brandId;
@property (nonatomic) NSString *productDescription;
@property (nonatomic) NSString *imageUri;
@property (nonatomic) NSString *searchName;
@property (nonatomic) NSNumber *hasImage;
@property (nonatomic) NSNumber *isStoreBrand;
@property (nonatomic) NSNumber *hasPrices;
@property (nonatomic) NSString *measureText;
@property (nonatomic) NSNumber *comparable;
@property (nonatomic) NSNumber *status;


@property (nonatomic) NSNumber *maxSavings;
@property (nonatomic) NSNumber *hasCoupons;
@property (nonatomic) NSNumber *isOnSale;
@property (nonatomic) NSNumber *winningStore;

@end
