//
//  IKOObject.h
//  uGrocery
//
//  Created by Duane Schleen on 7/6/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

@interface FMap : NSObject

@property (nonatomic) NSString *keyPath;
@property (nonatomic) NSString *jsonKey;
@property (nonatomic) Class klass;
@property (nonatomic) BOOL convertNumberToString;

+ (instancetype)mapPropertyName:(NSString *)name;
+ (instancetype)mapPropertyName:(NSString *)name
  toJsonKey:(NSString *)jsonKey;
- (instancetype)convertNumberToString:(BOOL)convertNumber;
- (instancetype)class:(Class)class;
- (instancetype)collectionClass:(Class)class;

@end


@interface IKOObject : NSObject <NSCoding>

+ (instancetype)objectFromJSON:(NSDictionary *)json;
- (NSDictionary *)JSONRepresentation;
+ (NSArray *)maps;

@end
