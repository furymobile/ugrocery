//
//  IKOProductCategoryJoin.m
//  uGrocery
//
//  Created by Duane Schleen on 7/12/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOProductCategoryJoin.h"

@implementation IKOProductCategoryJoin

+ (NSArray *)maps {
    return @[
        [FMap mapPropertyName:@"uuid" toJsonKey:@"uuid"],
        [FMap mapPropertyName:@"parentId" toJsonKey:@"ParentId"],
        [FMap mapPropertyName:@"childId" toJsonKey:@"ChildId"]
    ];
}

@end
