//
//  IKOProductCategoryJoin.h
//  uGrocery
//
//  Created by Duane Schleen on 7/12/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOObject.h"

@interface IKOProductCategoryJoin : IKOObject

@property (nonatomic) NSString *uuid;
@property (nonatomic) NSNumber *parentId;
@property (nonatomic) NSNumber *childId;

@end
