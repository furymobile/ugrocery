//
//  IKOFLBRequestProduct.m
//  uGrocery
//
//  Created by Duane Schleen on 8/31/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOFLBRequestProduct.h"
#import "IKOShoppingListItem.h"
#import "IKOProduct.h"
#import "IKOFavoriteItem.h"

@implementation IKOFLBRequestProduct

- (instancetype)initWithShoppingListItem:(IKOShoppingListItem *)shoppingListItem {
    self = [super init];
    if (self) {
        self.productId = shoppingListItem.productId;
        self.quantity = shoppingListItem.quantity;
    }
    return self;
}

- (instancetype)initWithProduct:(IKOProduct *)product {
    self = [super init];
    if (self) {
        self.productId = product.productId;
        self.upc = product.upc;
        self.quantity = @1;
    }
    return self;
}

- (instancetype)initWithUpc:(NSString *)upc {
    self = [super init];
    if (self) {
        self.productId = @0;
        self.upc = upc;
        self.quantity = @1;
    }
    return self;
}

- (instancetype)initWithMyProductItem:(IKOFavoriteItem *)myProductItem {
    self = [super init];
    if (self) {
        self.productId = myProductItem.productId;
        self.quantity = @1;
    }
    return self;
}

+ (NSArray *)maps {
    return @[
        [FMap mapPropertyName:@"productId" toJsonKey:@"ProductId"],
        [FMap mapPropertyName:@"quantity" toJsonKey:@"Quantity"],
        [FMap mapPropertyName:@"upc" toJsonKey:@"Upc"]
    ];
}

@end
