//
//  IKOPriceUpdate.h
//  uGrocery
//
//  Created by Duane Schleen on 9/18/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IKOObject.h"

@class IKOUser;
@class IKOProduct;

@interface IKOPriceUpdate : IKOObject

@property (nonatomic) NSString *priceUpdateId;
@property (nonatomic) NSString *upc;
@property (nonatomic) NSNumber *captureDate;
@property (nonatomic) NSNumber *userId;
@property (nonatomic) NSNumber *latitude;
@property (nonatomic) NSNumber *longitude;
@property (nonatomic) NSNumber *l3Id;
@property (nonatomic) NSNumber *storeId;
@property (nonatomic) NSString *productDescription;
@property (nonatomic) NSString *size;
@property (nonatomic) NSNumber *brandId;
@property (nonatomic) NSNumber *productId;
@property (nonatomic) NSString *brandName;
@property (nonatomic) NSNumber *ppfSafe;
@property (nonatomic) NSNumber *price;
@property (nonatomic) NSNumber *salePrice;
@property (nonatomic) NSString *saleDescription;
@property (nonatomic) NSNumber *saleEndDate;

- (instancetype)initWithUPC:(NSString *)upc
  andUser:(IKOUser *)user;

- (instancetype)initWithProduct:(IKOProduct *)product
  andUser:(IKOUser *)user;

- (NSDictionary *)submissionPayload;

@end
