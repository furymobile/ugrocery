//
//  IKOUser.m
//  uGrocery
//
//  Created by Duane Schleen on 8/18/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOUser.h"

@implementation IKOUser

+ (NSArray *)maps {
    return @[
        [FMap mapPropertyName:@"userId" toJsonKey:@"UserId"],
        [FMap mapPropertyName:@"userTypeId" toJsonKey:@"UserTypeId"],
        [FMap mapPropertyName:@"loginEmail" toJsonKey:@"LoginEmail"],
        [FMap mapPropertyName:@"firstName" toJsonKey:@"FirstName"],
        [FMap mapPropertyName:@"lastName" toJsonKey:@"LastName"],
        [FMap mapPropertyName:@"zipCode" toJsonKey:@"zipCode"],
        [FMap mapPropertyName:@"phone" toJsonKey:@"Phone"],
        [FMap mapPropertyName:@"oauthToken" toJsonKey:@"oauth_token"],
        [FMap mapPropertyName:@"oauthSecret" toJsonKey:@"oauth_token_secret"]
    ];
}

@end
