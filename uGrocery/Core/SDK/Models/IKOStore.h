//
//  IKOStore.h
//  uGrocery
//
//  Created by Duane Schleen on 10/30/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOObject.h"

@interface IKOStore : IKOObject

@property (nonatomic) NSNumber *storeId;
@property (nonatomic) NSString *name;

- (instancetype)initWithId:(NSNumber *)storeId andName:(NSString *)name;

@end
