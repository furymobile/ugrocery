//
//  FTimer.h
//  Copyright (c) 2013 Fury Mobile. All rights reserved.
//
//  License:
//
//  Apache License
//  Version 2.0, January 2004
//  http://www.apache.org/licenses/
//

#import <Foundation/Foundation.h>

@interface FTimer : NSObject

- (id)initWithTimeInterval:(NSTimeInterval)seconds andBlock:(void (^)(void))block;

- (void)pause;
- (void)resume;
- (void)fire;
- (void)invalidate;

@end

