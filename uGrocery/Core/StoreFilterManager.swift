//
//  StoreFilterManager.swift
//  uGrocery
//
//  Created by Todd Goldenbaum on 8/27/16.
//  Copyright © 2016 ikonomo. All rights reserved.
//

import UIKit

@objc protocol FilterDelegate {
    func reloadDataToReflectFilterChanges()
    func updateFilterButtonImage()
}

class StoreFilterManager: NSObject {

    // MARK: - Object Life Cycle

    static let shared = StoreFilterManager()

    // Non-table-driven prototype - replace w/nsuserdefaults dictionary etc.
    var kingSooperFilter = true
    var safewayFilter    = true
    var targetFilter     = true
    var walmartFilter    = true

    override init () {
        super.init()
        loadFilters()
    }
    
    func loadFilters() {
    
        if let persistedFilters = UserDefaults.standard.array(forKey: "storefilters") as? [String] {
            
            kingSooperFilter = persistedFilters.contains("2")
            safewayFilter = persistedFilters.contains("4")
            targetFilter = persistedFilters.contains("5")
            walmartFilter = persistedFilters.contains("7")
                
        }
    }
    
    func saveFilters() {
        UserDefaults.standard.set(selectedStoreIds(), forKey: "storefilters")
    }

    // OBjective-C-friendly interface (sans swift structs returned)
    // Post-prototype: derive and return keys from dictionary as an array (remember the keys are actually numbers-as-strings)
    func selectedStoreIds() -> [String] {
        var ids = [String]()
        if kingSooperFilter {
            ids.append("2")
        }
        if safewayFilter {
            ids.append("4")
        }
        if targetFilter {
            ids.append("5")
        }
        if walmartFilter {
            ids.append("7")
        }
        return ids
    }

    func filterButtonImage() -> UIImage {
        var count = StoreFilterManager.shared.selectedStoreIds().count

        // All filters on means there's no filtering at all
        if count == 4 {
            count = 0
        }

        let image = UIImage(named: "filter_\(count)")!
        return image.withRenderingMode(.alwaysOriginal)
    }

    func filterType() -> String {
        return "store"
    }
}
