//
//  FTimer.m
//  Copyright (c) 2013 Fury Mobile. All rights reserved.
//
//  License:
//
//  Apache License
//  Version 2.0, January 2004
//  http://www.apache.org/licenses/
//

#import "FTimer.h"

@interface FTimer ()

@property (nonatomic, copy) dispatch_block_t block;
@property (nonatomic, retain) dispatch_source_t source;
@property (nonatomic, assign) BOOL isRunning;

@end

@implementation FTimer


- (id)initWithTimeInterval:(NSTimeInterval)seconds andBlock:(void (^)(void))block {
    NSParameterAssert(seconds);
    NSParameterAssert(block);

    self = [super init];
    if (self) {
        _block = block;
        _source = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,dispatch_get_main_queue());
        uint64_t nano = (uint64_t)(seconds * NSEC_PER_SEC);
        dispatch_source_set_timer(_source, dispatch_time(DISPATCH_TIME_NOW, nano), nano, 0);
        dispatch_source_set_event_handler(_source, block);
        dispatch_resume(_source);
        _isRunning = YES;
    }
    return self;
}

- (void)pause {
    if (_isRunning) {
        dispatch_suspend(_source);
        _isRunning = NO;
    }
}

- (void)resume {
    if (!_isRunning) {
        dispatch_resume(_source);
        _isRunning = YES;
    }
}

- (void)fire {
    self.block();
}

- (void)invalidate {
    if (_source) {
        dispatch_source_cancel(_source);
        _source = nil;
    }
    _block = nil;
}

- (void)dealloc {
    [self invalidate];
}

@end
