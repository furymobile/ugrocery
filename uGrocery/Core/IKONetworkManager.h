//
//  IKONetworkManager.h
//  uGrocery
//
//  Created by Duane Schleen on 3/18/15.
//  Copyright (c) 2015 ikonomo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IKONetworkManager : NSObject

+ (IKONetworkManager *)sharedInstance;
+ (BOOL)connectedToNetwork;

@end
