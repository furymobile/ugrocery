//
//  IKOUtilities.m
//  uGrocery
//
//  Created by Duane Schleen on 9/18/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOProduct.h"
#import "IKOUtilities.h"
#import "UIAlertView+Blocks.h"
#import "UIColor+uGrocery.h"
#import "UIFont+uGrocery.h"
#import "UIImage+ImageEffects.h"
#import "UIView+Image.h"
#import <AVFoundation/AVFoundation.h>

@implementation IKOUtilities

+ (NSAttributedString *)attributedLabelFor:(NSString *)description andMeasure:(NSString *)measure titleSize:(int)size {
    NSString *measureText = SF(@"\n%@", measure);
    NSMutableAttributedString *label = [[NSMutableAttributedString alloc]initWithString:SF(@"%@%@", description, (measure) ? measureText : @"")];

    // Declare the fonts
    UIFont *labelFont1 =  [UIFont IKOMediumFontOfSize:size];//[UIFont fontWithName:@"HelveticaNeue-Medium" size:size];
    UIFont *labelFont2 = [UIFont IKOLightFontOfSize:11.0];

    // Declare the colors
    UIColor *labelColor1 = [UIColor IKODarkGrayColor];
    UIColor *labelColor2 = [UIColor colorWithRed:0.000000 green:0.000000 blue:0.000000 alpha:0.80000];

    // Declare the paragraph styles
    NSMutableParagraphStyle *labelParaStyle1 = [[NSMutableParagraphStyle alloc]init];
    labelParaStyle1.alignment = 4;


    // Create the attributes and add them to the string
    [label addAttribute:NSForegroundColorAttributeName value:labelColor1 range:NSMakeRange(0,description.length)];
    [label addAttribute:NSParagraphStyleAttributeName value:labelParaStyle1 range:NSMakeRange(0,description.length)];
    [label addAttribute:NSFontAttributeName value:labelFont1 range:NSMakeRange(0,description.length)];
    [label addAttribute:NSForegroundColorAttributeName value:labelColor2 range:NSMakeRange(description.length+1,measure.length)];
    [label addAttribute:NSParagraphStyleAttributeName value:labelParaStyle1 range:NSMakeRange(description.length+1,measure.length)];
    [label addAttribute:NSFontAttributeName value:labelFont2 range:NSMakeRange(description.length+1,measure.length)];

    return label;
}

+ (void)animateAddToCartInView:(UIView *)targetView
  imageviewToAnimate:(UIImageView *)imageView
  animateToPoint:(CGPoint)endPoint {
    CGRect rect = [imageView convertRect:imageView.frame toView:targetView];

    UIImageView *starView = [[UIImageView alloc]initWithImage:imageView.image];
    starView.backgroundColor = [UIColor whiteColor];
    [starView setFrame:rect];
    starView.layer.cornerRadius=5;
    starView.layer.borderColor=[UIColor UGReallyLtGrey].CGColor;
    starView.layer.borderWidth=1;
    [targetView addSubview:starView];
    [[self class]animateView:starView fromPoint:CGPointMake(rect.origin.x, rect.origin.y) toPoint:endPoint];

    [starView performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:1];
}

+ (void)animateView:(UIView *)view fromPoint:(CGPoint)start toPoint:(CGPoint)end {
    // The animation
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"position"];

    // Animation's path
    UIBezierPath *path = [UIBezierPath bezierPath];
    [[self class]debugPath:path toView:view];

    // Move the "cursor" to the start
    [path moveToPoint:start];

    // Calculate the control points
    CGPoint c1 = CGPointMake(start.x + 64, start.y);
    CGPoint c2 = CGPointMake(end.x, end.y - 128);

    // Draw a curve towards the end, using control points
    [path addCurveToPoint:end controlPoint1:c1 controlPoint2:c2];

    // Use this path as the animation's path (casted to CGPath)
    animation.path = path.CGPath;

    // The other animations properties
    animation.fillMode              = kCAFillModeForwards;
    animation.removedOnCompletion   = NO;
    animation.duration              = .85;
    animation.timingFunction        = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];

    // Apply it
    [view.layer addAnimation:animation forKey:@"animation.trash"];
}

+ (void)debugPath:(UIBezierPath *)path toView:(UIView *)view {
    // Drawing the path
    CAShapeLayer *layer = [CAShapeLayer layer];
    layer.path          = path.CGPath;
    layer.strokeColor   = [UIColor blackColor].CGColor;
    layer.lineWidth     = 1.0;
    layer.fillColor     = nil;

    [view.layer addSublayer:layer];
}

+ (CGRect)frameForTabInTabBar:(UITabBar *)tabBar withIndex:(NSUInteger)index inView:(UIView *)targetView {
    NSUInteger currentTabIndex = 0;

    for (UIView *subView in tabBar.subviews) {
        if ([subView isKindOfClass:NSClassFromString(@"UITabBarButton")]) {
            if (currentTabIndex == index)
                return [subView.superview convertRect:subView.frame toView:targetView];
            else
                currentTabIndex++;
        }
    }

    NSAssert(NO, @"Index is out of bounds");
    return CGRectNull;
}

+ (BOOL)hasCameraPermission {
    BOOL __block accessGranted = NO;
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(authStatus == AVAuthorizationStatusAuthorized) {
        accessGranted = YES;
    } else if(authStatus == AVAuthorizationStatusDenied){
        accessGranted = NO;
    } else if(authStatus == AVAuthorizationStatusRestricted){
        accessGranted = NO;
    } else if(authStatus == AVAuthorizationStatusNotDetermined){
        dispatch_semaphore_t sem = dispatch_semaphore_create(0);
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            accessGranted = granted;
            dispatch_semaphore_signal(sem);
        }];
        dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
    }
    return accessGranted;
}

+ (UIImage *)blurView:(UIView *)view {
    UIImage *image = [view renderAsImage];
    return [image applySubtleLightEffect];
}

+ (BOOL)notConnectedAlert {
    if (![IKONetworkManager connectedToNetwork]) {
        [UIAlertView showWithTitle:@"No Connection"
                           message:@"Oops, currently you aren’t connected, please try again with an Internet connection."
                 cancelButtonTitle:@"OK"
                 otherButtonTitles:nil
                          tapBlock:nil];
        return true;
    }
    return false;
}

@end
