//
//  IKOAnaylitics.m
//  uGrocery
//
//  Created by Duane Schleen on 6/13/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#define ANALYTIC(event) [IKOAnalytics logEvent : event withParameters : nil]
#define ANALYTIC_PARAMS(event,params) [IKOAnalytics logEvent : event withParameters : params]
#define ANALYTIC_ERROR(event, error) [IKOAnalytics logError : event withMessage : error.localizedDescription andError : error]

#define ANALYTICS_EVENT_STARTED_APP @"Started uGrocery"

#import <Foundation/Foundation.h>

@class CLLocation;

@interface IKOAnalytics : NSObject

+ (IKOAnalytics *)sharedInstance;

+ (void)setLocation:(CLLocation *)location;

+ (void)logEvent:(NSString *)eventName
  withParameters:(NSDictionary *)parameters;

#pragma mark - Errors and Exceptions

+ (void)logException:(NSString *)eventName
  withMessage:(NSString *)message
  andException:(NSException *)exception;

+ (void)logError:(NSString *)eventName
  withMessage:(NSString *)message
  andError:(NSError *)error;

+ (void)logError:(NSString *)description
  withErrorActivity:(NSString *)errorActivity;

#pragma mark - One Time Events

+ (void)logOneTimeEvent:(NSString *)eventName
  withParameters:(NSDictionary *)parameters;

#pragma mark - Activated Event

+ (void)logActivatedEvent;

+ (void)logApptentiveEvent:(NSString *)eventName fromViewController:viewController;

@end
