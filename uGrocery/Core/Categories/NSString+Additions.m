//
//  NSString+Additions.m
//  uGrocery
//
//  Created by Duane Schleen on 1/22/15.
//  Copyright (c) 2015 ikonomo. All rights reserved.
//

#import "NSString+Additions.h"

@implementation NSString (Additions)

- (NSString *)stripPunctuation
{
    return [[self componentsSeparatedByCharactersInSet:[[NSCharacterSet alphanumericCharacterSet] invertedSet]] componentsJoinedByString:@""];
}

- (NSArray *)arrayBySplittingIntoWords
{
    NSMutableArray *words = [NSMutableArray array];
    NSScanner *scanner = [NSScanner scannerWithString:self];
    NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    
    NSString *word;
    while ([scanner scanUpToCharactersFromSet:set intoString:&word]) {
        [words addObject:word];
    }
    
    return words;
}


@end
