//
//  UIView+CenteredSectionTitle.m
//  uGrocery
//
//  Created by Duane Schleen on 8/19/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "UIView+CenteredSectionTitle.h"
#import "UIColor+uGrocery.h"
#import "UIFont+uGrocery.h"
#import "UIScreen+Additions.h"

@implementation UIView (CenteredSectionTitle)


+ (UIView *)viewWithSectionHeader:(NSString *)title
{
	UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen currentSize].width, 28)];
    view.bounds = view.frame;
	view.backgroundColor = [UIColor clearColor];
	UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(9, 6, 300, 18)];
    label.textAlignment = NSTextAlignmentLeft;
	[label setBackgroundColor:[UIColor clearColor]];
	[label setFont:[UIFont IKOMediumFontOfSize:14]];
    [label setTextColor:[UIColor UGBlue]];
	label.text = [title lowercaseString];
	view.clipsToBounds = YES;
	[view addSubview:label];
	return view;
}

+ (UIView *)favoriteSectionHeader:(NSString *)title target:(id)target selector:(SEL)selector
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen currentSize].width, 18)];
    view.bounds = view.frame;
    view.backgroundColor = [UIColor clearColor];
    
    UIImageView *star = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon-heart-selected"]];
    star.frame = CGRectMake(9, 5, 14, 14);
    
    UIButton *addAll = [[UIButton alloc] initWithFrame:CGRectMake([UIScreen currentSize].width - 103, 0, 100, 28)];
    [addAll addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    [addAll setTitle:@"add favorites" forState:UIControlStateNormal];
    [addAll setTitleColor:[UIColor IKOOrangeColor] forState:UIControlStateNormal];
    addAll.titleLabel.font = [UIFont IKOMediumFontOfSize:14];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(27, 6, 200, 14)];
    label.textAlignment = NSTextAlignmentLeft;
    [label setBackgroundColor:[UIColor clearColor]];
    [label setFont:[UIFont IKOMediumFontOfSize:15]];
    [label setTextColor:[UIColor UGBlue]];
    label.text = [title lowercaseString];
    view.clipsToBounds = YES;
    
    [view addSubview:addAll];
    [view addSubview:star];
    [view addSubview:label];
    return view;
}

@end
