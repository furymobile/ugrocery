//
//  UIView+CenteredSectionTitle.h
//  uGrocery
//
//  Created by Duane Schleen on 8/19/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIView (CenteredSectionTitle)

+ (UIView *)viewWithSectionHeader:(NSString *)title;
+ (UIView *)favoriteSectionHeader:(NSString *)title target:(id)target selector:(SEL)selector;

@end
