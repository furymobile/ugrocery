//
//  UINavigationBar+Additions.h
//  UINavigationBar+Additions
//
//  Created by Junda on 14/3/14.
//  Copyright (c) 2014 Just2us. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationBar (Additions)

- (void)hideBottomHairline;
- (void)showBottomHairline;

@end
