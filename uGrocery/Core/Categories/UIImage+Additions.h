//
//  UIImage+Additions.h
//  Copyright (c) 2014 Fury Mobile. All rights reserved.
//
//  License:
//
//  Apache License
//  Version 2.0, January 2004
//  http://www.apache.org/licenses/
//


#import <Foundation/Foundation.h>

@interface UIImage (Additions)

+ (UIImage *)imageFromColor:(UIColor *)color;
- (UIImage *)imageWithColor:(UIColor *)color;


@end
