//
//  UIView+Animation.m
//  Copyright (c) 2014 Fury Mobile. All rights reserved.
//
//  License:
//
//  Apache License
//  Version 2.0, January 2004
//  http://www.apache.org/licenses/
//

#import "UIView+Animation.h"

@implementation UIView (Animation)

- (void)spinWithDuration:(CGFloat)duration
                     rotations:(CGFloat)rotations
                        repeat:(float)repeat
{
    CABasicAnimation *rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat:M_PI * 2.0 /* full rotation*/ * rotations * duration];
    rotationAnimation.duration = duration;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = repeat;

    [self.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

@end
