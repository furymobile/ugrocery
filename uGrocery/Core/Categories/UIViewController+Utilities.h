//
//  UIViewController+Utilities.h
//  fury-utilities
//
//  Created by Duane Schleen on 8/3/14.
//  Copyright (c) 2014 Fury Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIViewController (Utilities)

- (void)setupViewController;

@end
