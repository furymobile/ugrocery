//
//  UIColor+uGrocery.m
//  uGrocery
//
//  Created by Duane Schleen on 6/10/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "UIColor+uGrocery.h"

@implementation UIColor (uGrocery)

#pragma mark - New Colors

#pragma mark - New Colors

+ (UIColor*)UGBlue
{
    return RGB(4, 135, 201);
}

+ (UIColor*)UGLtBlue
{
    return RGB(105, 188, 228);
}

+ (UIColor*)UGDkGrey
{
    return RGB(56, 56, 56);
}

+ (UIColor*)UGLtGrey
{
    return RGB(147, 147, 147);
}

+ (UIColor*)UGReallyLtGrey
{
    return RGB(239, 239, 239);
}

+ (UIColor*)UGWhite
{
    return RGB(255, 255, 255);
}

+ (UIColor*)UGRed
{
    return RGB(255, 0, 0);
}

+ (UIColor*)UGGreen
{
    return RGB(31, 175, 44);
}


#pragma mark - Old Colors

+ (UIColor*)IKOErrorLogColor
{
	return [UIColor redColor];
}

+ (UIColor*)IKOInfoLogColor
{
	return [UIColor cyanColor];
}

+ (UIColor*)IKOWarnLogColor

{
	return [UIColor orangeColor];
}

+ (UIColor*)IKOVerboseLogColor

{
	return [UIColor lightGrayColor];
}

+ (UIColor*)IKOBlueColor
{
	return RGB(12,82,121);
}

+ (UIColor*)IKOGreenColor
{
	return RGB(83, 155, 21);
}

+ (UIColor*)IKOOrangeColor
{
    return RGB(247, 148, 29);
}


+ (UIColor*)IKODarkGrayColor
{
	return RGB(127, 127, 127);
}

+ (UIColor*)IKOLightGrayColor
{
	return RGB(127, 127, 127);
}

+ (UIColor*)IKOBackgroundColor
{
    return [UIColor colorWithRed:0.965 green:0.969 blue:0.969 alpha:1.000];
}

+ (UIColor*)IKODirtyWhiteColor
{
    return [UIColor colorWithRed:0.96 green:0.96 blue:0.96 alpha:1];
}

+ (UIColor*)IKODividerColor
{
    return [UIColor colorWithWhite:0.902 alpha:1.000];
}




@end
