//
//  NSArray+Additions.m
//  uGrocery
//
//  Created by Duane Schleen on 1/22/15.
//  Copyright (c) 2015 ikonomo. All rights reserved.
//

#import "NSArray+Additions.h"

@implementation NSArray (Additions)

- (NSArray *)sortedArray {
    return [self sortedArrayUsingSelector:@selector(compare:)];
}

- (id)firstElementMatchingBlock:(BOOL(^)(id))block {
    for (id element in self) {
        if (block(element))
            return element;
    }
    return nil;
}

- (void)splitAt:(NSUInteger)idx left:(NSArray **)left right:(NSArray **)right{
    NSUInteger actualIdx = MIN(idx, self.count);
    if (left)
        *left = [self subarrayWithRange:NSMakeRange(0, actualIdx)];
    if (right)
        *right = [self subarrayWithRange:NSMakeRange(actualIdx, self.count - actualIdx)];
}

- (void)splitWhere:(BOOL(^)(id element, NSUInteger idx, BOOL* stop))block left:(NSArray **)left right:(NSArray **)right {
    [self splitAt:[self indexOfObjectPassingTest:block] left:left right:right];
}

- (NSMutableArray *)filteredArrayUsingBlock:(BOOL(^)(id))block{
    NSMutableArray *result = [NSMutableArray array];
    for (id element in self) {
        if (block(element))
            [result addObject:element];
    }
    return result;
}

@end
