//
//  UIFont+uGrocery.h
//  uGrocery
//
//  Created by Duane Schleen on 6/10/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIFont (uGrocery)

+ (void)logAvailableFonts;

+ (UIFont*)IKOLightFontOfSize:(CGFloat)fontSize;

+ (UIFont*)IKOMediumFontOfSize:(CGFloat)fontSize;

+ (UIFont*)IKOHeavyFontOfSize:(CGFloat)fontSize;

+ (UIFont*)IKOItalicFontOfSize:(CGFloat)fontSize;

@end
