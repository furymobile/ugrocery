//
//  UIScreen+Additions.h
//  Copyright (c) 2014 Fury Mobile. All rights reserved.
//
//  License:
//
//  Apache License
//  Version 2.0, January 2004
//  http://www.apache.org/licenses/
//

#import <Foundation/Foundation.h>

@interface UIScreen (Additions)

+ (CGSize)currentSize;
+ (CGFloat)currentScale;


@end
