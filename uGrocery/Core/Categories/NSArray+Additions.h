//
//  NSArray+Additions.h
//  uGrocery
//
//  Created by Duane Schleen on 1/22/15.
//  Copyright (c) 2015 ikonomo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Additions)

- (NSArray *)sortedArray;
- (id)firstElementMatchingBlock:(BOOL(^)(id))block;
- (void)splitWhere:(BOOL(^)(id element, NSUInteger idx, BOOL* stop))block left:(NSArray **)left right:(NSArray **)right;
- (NSMutableArray*)filteredArrayUsingBlock:(BOOL (^)(id))block;

@end
