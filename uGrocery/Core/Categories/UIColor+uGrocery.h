//
//  UIColor+uGrocery.h
//  uGrocery
//
//  Created by Duane Schleen on 6/10/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#define RGB(r, g, b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1]
#define RGBA(r, g, b, a) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:a]

#import <Foundation/Foundation.h>

@interface UIColor (uGrocery)

#pragma mark - New Colors

+ (UIColor*)UGBlue;
+ (UIColor*)UGLtBlue;
+ (UIColor*)UGDkGrey;
+ (UIColor*)UGLtGrey;
+ (UIColor*)UGReallyLtGrey;
+ (UIColor*)UGWhite;
+ (UIColor*)UGRed;
+ (UIColor*)UGGreen;

#pragma mark - Old Colors

+ (UIColor*)IKOErrorLogColor;
+ (UIColor*)IKOInfoLogColor;
+ (UIColor*)IKOWarnLogColor;
+ (UIColor*)IKOVerboseLogColor;

//+ (UIColor*)IKOBlueColor;
//+ (UIColor*)IKOGreenColor;
+ (UIColor*)IKOOrangeColor;
+ (UIColor*)IKODarkGrayColor;
+ (UIColor*)IKOLightGrayColor;
+ (UIColor*)IKOBackgroundColor;
//+ (UIColor*)IKODirtyWhiteColor;
+ (UIColor*)IKODividerColor;

@end
