//
//  NSEnumerator+Contains.h
//  uGrocery
//
//  Created by Duane Schleen on 1/22/15.
//  Copyright (c) 2015 ikonomo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSEnumerator (Contains)

- (BOOL)contains:(id)value;

@end
