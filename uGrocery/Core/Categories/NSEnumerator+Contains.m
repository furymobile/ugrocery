//
//  NSEnumerator+Contains.m
//  uGrocery
//
//  Created by Duane Schleen on 1/22/15.
//  Copyright (c) 2015 ikonomo. All rights reserved.
//

#import "NSEnumerator+Contains.h"

@implementation NSEnumerator (Contains)

- (BOOL)contains:(id)value
{
    return [self anyElementMatchesBlock:^BOOL(id element) {
        return [element isEqual:value];
    }];
}

- (BOOL)anyElementMatchesBlock:(BOOL(^)(id))block
{
    for (id object in self) {
        if (block(object)) return YES;
    }
    return NO;
}

@end
