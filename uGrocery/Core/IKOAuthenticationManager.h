//
//  IKOAuthenticationManager.h
//  uGrocery
//
//  Created by Duane Schleen on 6/13/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#define ALL_PRODUCT_IDS IKO1MonthSubscription, IKO3MonthSubscription, IKO1YearSubscription
#define kSS @"dd6613c826504167aca80dd9a3e4660a"

typedef NS_ENUM (NSInteger, IKOUserRoles) {
    IKORoleNone = 0,
    IKORoleMember = 1,
    IKORoleUpdater = 2,
    IKORoleAdministrator = 4,
    IKORoleFreeMember = 6,
    IKORoleDeveloper = 7,
    IKORoleuGroceryCustomer = 8,
    IKORoleuGroceryUpdater = 9,
    IKORoleuGroceryOnlineMember = 10
};

UIKIT_EXTERN NSString *const IKOAuthenticationManagerDidAuthenticate;
UIKIT_EXTERN NSString *const IKOAuthenticationManagerDidSignOut;

UIKIT_EXTERN NSString *const IKO1MonthSubscription;
UIKIT_EXTERN NSString *const IKO3MonthSubscription;
UIKIT_EXTERN NSString *const IKO6MonthSubscription;
UIKIT_EXTERN NSString *const IKO1YearSubscription;

UIKIT_EXTERN NSString *const kSubscriptionExpirationDateKey;

UIKIT_EXTERN const NSString *kFirstLaunchKey;

#import <Foundation/Foundation.h>
#import "IKOUser.h"

@class UVConfig;
@class IKOServerResponse;
@class IKOUser;

@interface IKOAuthenticationManager : NSObject

@property (nonatomic) BOOL showFirstLaunchDialog;

+ (IKOAuthenticationManager *)sharedInstance;
+ (IKOUser *)currentUser;

- (BOOL)isInTrialPeriod;
- (long)trialPeriodRemaining;
- (void)authenticatedUser:(IKOServerResponse *)response;
- (BOOL)isAuthenticated;
- (BOOL)isAdministrator;
- (BOOL)isUpdater;
- (BOOL)isPremium;
- (void)signOut;

- (UVConfig *)userVoiceConfig;

- (int)daysRemainingOnSubscription;
- (NSString *)getExpirationDateString;
- (NSDate *)getExpirationDateForMonths:(int)months;
- (void)purchaseSubscriptionWithMonths:(int)months;
- (NSDate *)expirationDate;

@end
