//
//  IKONewHomeViewController.h
//  uGrocery
//
//  Created by Duane Schleen on 2/20/16.
//  Copyright © 2016 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IKOBarcodeScanner.h"

@interface IKOHomeVC : UITableViewController <IKOBarcodeScannerDelegate>


@end
