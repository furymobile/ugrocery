//
//  IKOBarcodeScanner.h
//  uGrocery
//
//  Created by Duane Schleen on 6/9/14.
//  Copyright (c) 2014 FullContact. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@class IKOProduct;

@protocol IKOBarcodeScannerDelegate <NSObject>

@required
- (void)closeScanner;
- (void)showProductDetailsFor:(IKOProduct *)product;

@end

@interface IKOBarcodeScanner : UIViewController

@property (nonatomic) id<IKOBarcodeScannerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *optionsView;
@property (weak, nonatomic) IBOutlet UIView *previewView;
@property (weak, nonatomic) IBOutlet UISwitch *autoAddSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *autoAddMyProductsSwitch;
@property (weak, nonatomic) IBOutlet UIView *target;
@property (weak, nonatomic) IBOutlet UIView *highlightView;
@property (weak, nonatomic) IBOutlet UIImageView *barcodeGhost;
@property (weak, nonatomic) IBOutlet UIImageView *spinner;

@end
