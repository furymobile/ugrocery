//
//  StoreFilterVC.swift
//  uGrocery
//
//  Created by Todd Goldenbaum on 8/31/16.
//  Copyright © 2016 ikonomo. All rights reserved.
//

import UIKit

@objc class StoreFilterVC: UIViewController {

    var delegate: FilterDelegate?

    @IBOutlet weak var kingSoopersCheckBox: CheckBox!
    @IBOutlet weak var safewayCheckBox: CheckBox!
    @IBOutlet weak var targetCheckBox: CheckBox!
    @IBOutlet weak var walmartCheckBox: CheckBox!
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var doneButton: UIButton!

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        kingSoopersCheckBox.isChecked   = StoreFilterManager.shared.kingSooperFilter
        safewayCheckBox.isChecked       = StoreFilterManager.shared.safewayFilter
        targetCheckBox.isChecked        = StoreFilterManager.shared.targetFilter
        walmartCheckBox.isChecked       = StoreFilterManager.shared.walmartFilter
    }

    func showNoStoresAlert() {
        let alertController = UIAlertController(title: "", message: "At least one store must be selected.", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { (result : UIAlertAction) -> Void in }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }

    func tooFewStoresSelected(sender: CheckBox) -> Bool {
        print("sender is \(sender.isChecked) and count is \(StoreFilterManager.shared.selectedStoreIds().count)")
        if sender.isChecked == true && StoreFilterManager.shared.selectedStoreIds().count == 1 {
            showNoStoresAlert()
            sender.isChecked = !sender.isChecked
            return true
        }
        return false
    }
    
    @IBAction func kingSoopersCheckBoxAction(_ sender: AnyObject) {
        if tooFewStoresSelected(sender: sender as! CheckBox) {
            return
        }
        StoreFilterManager.shared.kingSooperFilter = !kingSoopersCheckBox.isChecked
    }

    @IBAction func safewayCheckBoxAction(_ sender: AnyObject) {
        if tooFewStoresSelected(sender: sender as! CheckBox) {
            return
        }
        StoreFilterManager.shared.safewayFilter = !safewayCheckBox.isChecked
    }

    @IBAction func targetCheckBoxAction(_ sender: AnyObject) {
        if tooFewStoresSelected(sender: sender as! CheckBox) {
            return
        }
        StoreFilterManager.shared.targetFilter = !targetCheckBox.isChecked
    }

    @IBAction func walmartCheckBoxAction(_ sender: AnyObject) {
        if tooFewStoresSelected(sender: sender as! CheckBox) {
          //  walmartCheckBox.isChecked = true
            return
        }
        StoreFilterManager.shared.walmartFilter = !walmartCheckBox.isChecked
    }

    @IBAction func doneButtonAction(_ sender: AnyObject) {
        StoreFilterManager.shared.saveFilters()
        self.willMove(toParentViewController: nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
        delegate!.updateFilterButtonImage()
        delegate?.reloadDataToReflectFilterChanges()
    }
    
    @IBAction func cancelButtonAction(_ sender: AnyObject) {
        StoreFilterManager.shared.loadFilters()
        self.willMove(toParentViewController: nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
        delegate!.updateFilterButtonImage()
    }
}
