//
//  IKOChangePasswordViewController.m
//  uGrocery
//
//  Created by Duane Schleen on 12/30/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOChangePasswordViewController.h"
#import "UIAlertView+Blocks.h"
#import "UIColor+uGrocery.h"
#import "IKOSDK.h"
#import "IKOAuthenticationManager.h"

@interface IKOChangePasswordViewController ()

@end

@implementation IKOChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    _oldPassword.delegate = self;
    _changedPassword.delegate = self;
    _confirmChangedPassword.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated {
    ANALYTIC(@"Opened Change Password Screen");
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self performSelector:@selector(becomeFirstResponder) withObject:_oldPassword afterDelay:1];
}

#pragma mark =- UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == _oldPassword) {
        [_changedPassword becomeFirstResponder];
    } else if (textField == _changedPassword) {
        [_confirmChangedPassword becomeFirstResponder];
    } else if (textField == _confirmChangedPassword) {
        [self send:self];
    }
    return YES;
}

#pragma mark - Validation

- (BOOL)isValid {
    if (_oldPassword.text.length == 0) {
        [UIAlertView showWithTitle:@"Old Password"
        message:@"Please enter your current password."
        cancelButtonTitle:@"OK"
        otherButtonTitles:nil
        tapBlock:nil];
        return NO;
    }

    if (_changedPassword.text.length < 8) {
        [UIAlertView showWithTitle:@"New Password"
        message:@"New password must be at least 8 characters (case sensitive)."
        cancelButtonTitle:@"OK"
        otherButtonTitles:nil
        tapBlock:nil];
        return NO;
    }

    if (![_changedPassword.text isEqualToString:_confirmChangedPassword.text]) {
        [UIAlertView showWithTitle:@"Password Mismatch"
        message:@"The Confirm Password does not match the New Password that was entered."
        cancelButtonTitle:@"OK"
        otherButtonTitles:nil
        tapBlock:nil];
        return NO;
    }

    return YES;
}

- (IBAction)send:(id)sender {
    if ([self isValid]) {
        ANALYTIC(@"Changed Password");
        [[IKOSDK new]changePassword:_oldPassword.text
        newPassword:_changedPassword.text
        user:[IKOAuthenticationManager currentUser]
        callback:^(IKOError *error, IKOServerResponse *response) {
            if (response.success) {
                [UIAlertView showWithTitle:@"Password Changed"
                message:@"Your password has been successfully changed"
                cancelButtonTitle:@"OK"
                otherButtonTitles:nil
                tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                    if (buttonIndex == 1) {
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                }];
            } else {
                [UIAlertView showWithTitle:@"Error Changing Password"
                message:SF(@"We were unable to change your password at this time.")
                cancelButtonTitle:@"OK"
                otherButtonTitles:nil
                tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                    if (buttonIndex == 1) {
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                }];
            }
        }];
    }
}

@end
