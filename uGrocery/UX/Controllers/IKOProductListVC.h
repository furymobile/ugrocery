//
//  IKOProductListVC.h
//  uGrocery
//
//  Created by Duane Schleen on 2/21/16.
//  Copyright © 2016 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IKOAddToShoppingListProtocol.h"

@class IKOProductCategory;

@interface IKOProductListVC : UIViewController <UITableViewDelegate, UITableViewDataSource, IKOAddToShoppingListProtocol>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *scrollerView;
@property (nonatomic) IKOProductCategory *selectedCategory;

@end
