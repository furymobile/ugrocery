//
//  IKOForgotPasswordViewController.h
//  uGrocery
//
//  Created by Duane Schleen on 12/30/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKOForgotPasswordViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *emailAddress;
@property (weak, nonatomic) IBOutlet UIButton *send;

- (IBAction)send:(id)sender;

@end
