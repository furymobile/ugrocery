//
//  IKOLaunchViewController.m
//  uGrocery
//
//  Created by A750954 on 3/3/15.
//  Copyright (c) 2015 ikonomo. All rights reserved.
//

#import "IKOLaunchVC.h"
#import "IKODataStorageManager+uGrocery.h"
#import "UIView+Animation.h"

@implementation IKOLaunchVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [_spinner spinWithDuration:15 rotations:1 repeat:10000];
}

- (void)viewDidAppear:(BOOL)animated {
    [IKODataStorageManager initialize:^{
        [self performSegueWithIdentifier:@"launchApplication" sender:self];
    }];
}

@end