//
//  IKOZeroStateVC.swift
//  uGrocery
//
//  Created by Duane Schleen on 10/10/16.
//  Copyright © 2016 ikonomo. All rights reserved.
//

import UIKit

@objc class ZeroStateVC: UIViewController {
    
    @IBOutlet weak var screenImage: UIImageView!
    @IBOutlet weak var zeroStateTitle: UILabel!
    @IBOutlet weak var zeroStateMessage: UILabel!
    
}
