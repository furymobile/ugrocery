//
//  IKOTermsAndPrivacyViewController.m
//  uGrocery
//
//  Created by Duane Schleen on 8/3/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOTermsAndPrivacyViewController.h"
#import "UIViewController+Utilities.h"

@interface IKOTermsAndPrivacyViewController ()

@end

@implementation IKOTermsAndPrivacyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupViewController];
    [self showTermsAndConditions];
}

- (void)viewWillAppear:(BOOL)animated {
    ANALYTIC(@"Opened Terms and Conditions");
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)showTermsAndConditions {
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://ugrocery.com/terms/"] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:500];
    [_webView loadRequest:request];
}

- (void)showPrivacyPolicy {
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://ugrocery.com/privacy/"] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:500];
    [_webView loadRequest:request];
}

- (IBAction)selection:(id)sender {
    UISegmentedControl *segmentedControl = (UISegmentedControl *)sender;
    if (segmentedControl.selectedSegmentIndex == 0) {
        [self showTermsAndConditions];
    } else {
        [self showPrivacyPolicy];
    }
}

@end
