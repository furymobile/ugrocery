//
//  IKOObjectSelectorViewController.m
//  uGrocery
//
//  Created by Duane Schleen on 10/31/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOObjectSelectorVC.h"
#import "IKODataStorageManager+uGrocery.h"
#import "IKOSearchResultCell.h"
#import "UIScreen+Additions.h"

@interface IKOObjectSelectorVC ()

@property (nonatomic) UISearchBar *searchBar;
@property (nonatomic) NSArray *searchResults;

@end

@implementation IKOObjectSelectorVC {
    IKOSearchDataStore *_searchDataStore;
    IKOBrandDataStore *_brandDataStore;
    IKOProductCategoryDataStore *_productCategoryDataStore;
    IKOStoreDataStore *_storeDataStore;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _searchDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOSearchItem class]];
    _brandDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOBrand class]];
    _productCategoryDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOProductCategory class]];
    _storeDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOStore class]];
    [self buildUI];
}

- (void)buildUI {
    _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, [UIScreen currentSize].width, 44)];
    _searchBar.delegate = self;
    _searchBar.searchBarStyle = UISearchBarStyleMinimal;

    if (_klass == [IKOProductCategory class]) {
        self.title = @"L3 Category";
        _searchBar.placeholder = @"Search Categories";
        _searchResults = [_searchDataStore allObjectsForNonZeroColumn:@"CategoryId"];
        ANALYTIC(@"Opened Object Selector for L3 Selection");
    } else if (_klass == [IKOBrand class]) {
        self.title = @"Brand";
        _searchBar.placeholder = @"Search Brands";
        _searchResults = [_searchDataStore allObjectsForNonZeroColumn:@"BrandId"];
        ANALYTIC(@"Opened Object Selector for Brand Selection");
    } else if (_klass == [IKOStore class]) {
        self.title = @"Store";
        _searchResults = [_storeDataStore allObjects];
        ANALYTIC(@"Opened Object Selector for Store Selection");
    }

    if (_klass != [IKOStore class])
        [self.tableView setTableHeaderView:_searchBar];
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _searchResults.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    if (_klass == [IKOStore class]) {
        IKOStore *store = _searchResults[indexPath.row];
        cell.textLabel.text = store.name;
    } else {
        IKOSearchItem *item = _searchResults[indexPath.row];
        cell.textLabel.text = item.itemDescription;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_klass == [IKOStore class]) {
        IKOStore *store = _searchResults[indexPath.row];
        _completionBlock(store);
    } else {
        IKOSearchItem *item = _searchResults[(long)indexPath.row];
        if (!_completionBlock) return;
        if (_klass == [IKOProductCategory class]) {
            _completionBlock([_productCategoryDataStore getProductCategory:item.categoryId]);
        } else if (_klass == [IKOBrand class]) {
            _completionBlock([_brandDataStore getBrand:item.brandId]);
        }
    }
}

#pragma mark - Search Bar

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if (searchText.length == 0) {
        _searchResults = [_searchDataStore allObjectsForNonZeroColumn:(_klass == [IKOProductCategory class]) ? @"CategoryId" : @"BrandId"];
    } else {
        _searchResults = [_searchDataStore search:searchText forNonZeroColumn:(_klass == [IKOProductCategory class]) ? @"CategoryId" : @"BrandId"];
    }
    [self.tableView reloadData];
}

@end
