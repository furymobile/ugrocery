//
//  IKOLandingViewController.m
//  uGrocery
//
//  Created by Duane Schleen on 11/12/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOLandingVC.h"
#import "IKOAuthenticationManager.h"
#import "IKOSignInVC.h"
#import "IKORegisterUserVC.h"
#import "UIViewController+Utilities.h"
#import "UIAlertView+Blocks.h"
#import "IKOUtilities.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "IKOSDK.h"

@implementation IKOLandingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    if ([[IKOAuthenticationManager sharedInstance]isAuthenticated])
        [self performSegueWithIdentifier:@"userAuthenticated" sender:self];
    [self setupViewController];
//    FBSDKLoginButton *loginButton = [[FBSDKLoginButton alloc] init];
//    loginButton.readPermissions = @[@"email"];
//    loginButton.frame = CGRectMake(self.facebookView.frame.origin.x, self.facebookView.frame.origin.y, 100, 30 );
//    [self.facebookView addSubview:loginButton];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:NO];
//    
//    if ([FBSDKAccessToken currentAccessToken] && ![[IKOAuthenticationManager sharedInstance]isAuthenticated]) {  //have a facebook token but no ugrocery token
//        if ([[FBSDKAccessToken currentAccessToken].permissions containsObject:@"email"]) {
//            if ([FBSDKAccessToken currentAccessToken]) {
//                [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields":@"email"}]
//                 startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
//                     if (!error) {
//                         NSLog(@"fetched user:%@", result);
//                         NSString *emailAddress = result[@"email"];
//                         NSString *identifier = result[@"id"];
//                         
//                         [[IKOSDK new]registerNewUser:emailAddress
//                                            firstName:nil
//                                             lastName:nil
//                                             password:identifier
//                                           postalCode:@"80202"
//                                            promoCode:nil
//                                          phoneNumber:nil
//                                           newsletter:NO
//                                           referredBy:nil
//                                             callback:^(IKOError *error, IKOServerResponse *response) {
//                                                 if (response.success) {
//                                                     
//                                                     ANALYTIC(@"Successfully Registered");
//                                                     [[IKOAuthenticationManager sharedInstance]authenticatedUser:response];
//                                                     [self performSegueWithIdentifier:@"userAuthenticated" sender:self];
//                                                     
//                                                     
//                                                 } else {
//                                                     [[IKOSDK new]loginUser:emailAddress
//                                                                   password:identifier
//                                                                   callback:^(IKOError *error, IKOServerResponse *response) {
//                                                                       if (response.success) {
//                                                                           ANALYTIC(@"Successfully Signed In");
//                                                                           [[IKOAuthenticationManager sharedInstance]authenticatedUser:response];
//                                                                           [self performSegueWithIdentifier:@"userAuthenticated" sender:self];
//                                                                       } else {
//                                                                           ANALYTIC_ERROR(@"Unable to Sign In", error.error);
//                                                                           NSLog(@"Unable to sign in: %@", error.errorDescription);
//                                                                           
//                                                    
//                                                                           [UIAlertView showWithTitle:@"Unable to Sign In"
//                                                                                              message:SF(@"We were unable to sign in with the credentials supplied.  It is possible that you have an account already registered with the email address %@. %@", emailAddress, error.responseErrorDescription ? error.responseErrorDescription : @"")
//                                                                                    cancelButtonTitle:@"OK"
//                                                                                    otherButtonTitles:nil
//                                                                                             tapBlock:nil];
//                                                                       }
//                                                                   }];
//                                                 }
//                                             }];
//                         
//
//                     }
//                 }];
//            }
//        }
//
//
//      //
//    }
    
}

- (IBAction)signin:(id)sender {
    ANALYTIC(@"Tapped Sign In");
    if ([self checkForInternet])
        [self performSegueWithIdentifier:@"signin" sender:self];
}

- (IBAction)signup:(id)sender {
    ANALYTIC(@"Tapped Sign Up");
    if ([self checkForInternet])
        [self performSegueWithIdentifier:@"signup" sender:self];
}

- (BOOL)checkForInternet {
    if ([IKOUtilities notConnectedAlert])
        return NO;
    return YES;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier]isEqualToString:@"signin"]) {
        IKOSignInVC *vc = [segue destinationViewController];
        vc.completionBlock = ^{
            [self.navigationController popViewControllerAnimated:NO];
            if ([[IKOAuthenticationManager sharedInstance]isAuthenticated])
                [self performSegueWithIdentifier:@"userAuthenticated" sender:self];
        };
    } else if ([[segue identifier]isEqualToString:@"signup"]) {
        IKORegisterUserVC *vc = [segue destinationViewController];
        vc.completionBlock = ^{
            [self.navigationController popViewControllerAnimated:NO];
            if ([[IKOAuthenticationManager sharedInstance]isAuthenticated])
                [self performSegueWithIdentifier:@"userAuthenticated" sender:self];
        };
    }
}

@end
