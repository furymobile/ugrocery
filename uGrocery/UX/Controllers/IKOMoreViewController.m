//
//  IKOMoreViewController.m
//  uGrocery
//
//  Created by Duane Schleen on 8/9/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOMoreViewController.h"
#import "UIColor+uGrocery.h"
#import "UIViewController+Utilities.h"
#import "UserVoice.h"
#import "IKOAuthenticationManager.h"
#import "ATConnect.h"
#import "UVStyleSheet.h"

@implementation IKOMoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupViewController];
    [self.tableView setBackgroundColor:[UIColor UGReallyLtGrey]];
}


- (void)viewWillAppear:(BOOL)animated {
    _diagnosticsCell.hidden = YES;
    if ([[IKOAuthenticationManager sharedInstance] isAdministrator]) {
        _diagnosticsCell.hidden = NO;
    }
}

#pragma mark - Table view data source

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 2)
        [UserVoice presentUserVoiceInterfaceForParentViewController:self];
    if (indexPath.row == 3)
        [[ATConnect sharedConnection] presentMessageCenterFromViewController:self];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
