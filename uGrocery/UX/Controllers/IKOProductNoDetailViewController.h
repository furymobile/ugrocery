//
//  IKOProductNoDetailViewController.h
//  uGrocery
//
//  Created by Duane Schleen on 10/29/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IKOProductDetailBaseVC.h"
#import "IKOFieldCell.h"
#import "IKOPriceUpdateCell.h"

@interface IKOProductNoDetailViewController : IKOProductDetailBaseVC <UITextFieldDelegate, IKOFieldCellDelegate,  IKOPriceUpdateCellDelegate>

@property (weak, nonatomic) IBOutlet UIButton *addToShoppingList;

- (IBAction)addToShoppingList:(id)sender;

@end
