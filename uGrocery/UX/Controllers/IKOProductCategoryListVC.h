//
//  IKOProductCategoryListVC.h
//  uGrocery
//
//  Created by Duane Schleen on 2/21/16.
//  Copyright © 2016 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IKOProductCategory;

@interface IKOProductCategoryListVC : UIViewController  <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) IKOProductCategory *selectedCategory;

@end
