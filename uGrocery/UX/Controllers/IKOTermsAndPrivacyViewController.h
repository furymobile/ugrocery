//
//  IKOTermsAndPrivacyViewController.h
//  uGrocery
//
//  Created by Duane Schleen on 8/3/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKOTermsAndPrivacyViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIWebView *webView;

- (IBAction)selection:(id)sender;


@end
