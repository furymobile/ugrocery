//
//  IKODealViewController.h
//  uGrocery
//
//  Created by Duane Schleen on 9/2/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IKOProduct;

@interface IKODealViewController : UITableViewController

@property (nonatomic) IKOProduct *product;
@property (nonatomic) NSString *storeId;
@property (nonatomic) NSDictionary *saleData;
@property (nonatomic) NSArray *couponData;
@property (nonatomic) NSString *maxSavings;

@end
