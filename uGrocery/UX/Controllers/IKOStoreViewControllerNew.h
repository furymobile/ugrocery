//
//  IKOStoreViewController.h
//  uGrocery
//
//  Created by Duane Schleen on 7/26/15.
//  Copyright (c) 2015 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IKOSubscriptionCell.h"

@interface IKOStoreViewControllerNew : UITableViewController <IKOPurchaseProductDelegate>

@end
