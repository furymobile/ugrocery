//
//  IKODealsViewController.h
//  uGrocery
//
//  Created by Duane Schleen on 9/1/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

//typedef NS_ENUM(NSInteger, IKODealsViewState) {
//    IKODealsStateTop = 0,
//    IKODealsStateMine,
//};

#import <UIKit/UIKit.h>
#import "IKODealsProductCell.h"
#import "IKODealsHeaderView.h"

@interface IKODealsViewController : UIViewController <IKODealProductSelectionDelegate, UITableViewDataSource, UITableViewDelegate, IKODealsHeaderDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)refresh:(id)sender;

@end
