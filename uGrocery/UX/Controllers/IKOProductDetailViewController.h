//
//  IKOProductDetailViewController.h
//  uGrocery
//
//  Created by Duane Schleen on 7/28/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IKOProductDetailBaseVC.h"
#import "IKODealViewController.h"
#import "IKOPriceUpdateCell.h"

@interface IKOProductDetailViewController : IKOProductDetailBaseVC <IKOPriceUpdateCellDelegate>

- (void)updatePrice;

@end
