//
//  IKOFavoritesViewController.h
//  uGrocery
//
//  Created by Duane Schleen on 8/13/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOMyProductCell.h"
#import "SWRevealTableViewCell.h"
#import <UIKit/UIKit.h>

@interface IKOFavoritesViewController : UITableViewController <SWRevealTableViewCellDelegate,SWRevealTableViewCellDataSource, IKOMyProductSelectionDelegate>

@end
