//
//  IKODiagnosticsViewController.h
//  uGrocery
//
//  Created by Duane Schleen on 7/29/15.
//  Copyright (c) 2015 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKODiagnosticsViewController : UITableViewController

@property (weak, nonatomic) IBOutlet UILabel *productCount;
@property (weak, nonatomic) IBOutlet UILabel *l1Count;
@property (weak, nonatomic) IBOutlet UILabel *l2Count;
@property (weak, nonatomic) IBOutlet UILabel *l3Count;
@property (weak, nonatomic) IBOutlet UILabel *brandCount;

- (IBAction)seedBrands:(id)sender;
- (IBAction)seedCategories:(id)sender;
- (IBAction)seedProducts:(id)sender;

@end
