//
//  IKOTabBarController.h
//  uGrocery
//
//  Created by Duane Schleen on 8/9/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKOTabBarController : UITabBarController <UITabBarControllerDelegate>

- (void)launchStore;

@end
