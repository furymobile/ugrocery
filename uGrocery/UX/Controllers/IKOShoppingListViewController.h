//
//  IKOShoppingListViewController.h
//  uGrocery
//
//  Created by Duane Schleen on 7/18/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOFLBHeaderView.h"
#import "IKOManageHeaderView.h"
#import "IKOShoppingListCell.h"
#import "SWRevealTableViewCell.h"
#import <UIKit/UIKit.h>

@interface IKOShoppingListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, SWRevealTableViewCellDelegate, SWRevealTableViewCellDataSource, IKOCrossOffShoppingListItemProtocol, IKOFLBHeaderDelegate, IKOManageHeaderDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)search:(id)sender;

@end
