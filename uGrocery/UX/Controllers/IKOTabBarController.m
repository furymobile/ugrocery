//
//  IKOTabBarController.m
//  uGrocery
//
//  Created by Duane Schleen on 8/9/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOTabBarController.h"
#import "UIViewController+Utilities.h"
#import "IKODataStorageManager+uGrocery.h"
#import "UITabBarItem+Additions.h"
#import "UIFont+uGrocery.h"
#import "UIColor+uGrocery.h"
#import "UIImage+Additions.h"

@interface IKOTabBarController ()

@property (nonatomic) IKOSettingsDataStore *settingsDataStore;

@end

@implementation IKOTabBarController {
    UINavigationController *_lastNavigationController;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupViewController];

    _settingsDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOObject class]];
    self.delegate = self;

    [self.tabBar setTintColor:[UIColor whiteColor]];
    [[UITabBarItem appearance]setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, nil] forState:UIControlStateSelected];


    UIColor *unselectedColor = [UIColor lightGrayColor];

    [[UITabBarItem appearance]setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:unselectedColor, NSForegroundColorAttributeName, nil]
    forState:UIControlStateNormal];

    for(UITabBarItem *item in self.tabBar.items) {
        item.image = [[item.selectedImage imageWithColor:unselectedColor]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(shoppingListChanged:) name:IKONotificationShoppingListChanged object:nil];

    NSString *lastTabIndex = [_settingsDataStore objectForKey:kLastTabIndex];

    if (lastTabIndex != nil)
        [self setSelectedIndex:[lastTabIndex integerValue]];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

- (void)shoppingListChanged:(NSNotification *)notification {
    IKOShoppingListDataStore *shoppingListDataStore = notification.object;
    NSUInteger count = [shoppingListDataStore countofUncrossedItems];
    UITabBarItem *item = self.tabBar.items[3];
    if (count > 0) {
        [item setCustomBadgeValue:SF(@"%lu",(unsigned long)count) withFont:[UIFont IKOMediumFontOfSize:13] andFontColor:[UIColor UGBlue] andBackgroundColor:[UIColor whiteColor]];
    } else {
        item.badgeValue = nil;
    }
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
    if (tabBarController.selectedIndex > 0 && tabBarController.selectedIndex < 4) {
        [_settingsDataStore setObject:SF(@"%lu", (unsigned long)tabBarController.selectedIndex) forKey:kLastTabIndex];
    } else {
        [_settingsDataStore removeObjectForKey:kLastTabIndex];
    }
    for(UIViewController *foo in tabBarController.viewControllers) {
        if([foo isKindOfClass:[UINavigationController class]]) {
            UINavigationController *bar = (UINavigationController *)foo;
            [bar popToRootViewControllerAnimated:YES];
        }
    }
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
    return YES;
}

- (void)launchStore {
    [self setSelectedIndex:self.tabBar.items.count - 1];
    UINavigationController *controller = self.viewControllers[self.tabBar.items.count - 1];
    [controller loadView];
    UIViewController *vc = [[UIStoryboard storyboardWithName:@"iPhone" bundle:nil]instantiateViewControllerWithIdentifier:@"ikoStoreViewController"];
    [vc loadView];
    [controller pushViewController:vc animated:NO];
}

@end
