//
//  IKOMoreViewController.h
//  uGrocery
//
//  Created by Duane Schleen on 8/9/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKOMoreViewController : UITableViewController

@property (weak, nonatomic) IBOutlet UITableViewCell *premiumCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *diagnosticsCell;

@end
