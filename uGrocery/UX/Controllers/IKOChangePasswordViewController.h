//
//  IKOChangePasswordViewController.h
//  uGrocery
//
//  Created by Duane Schleen on 12/30/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKOChangePasswordViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *oldPassword;
@property (weak, nonatomic) IBOutlet UITextField *changedPassword;
@property (weak, nonatomic) IBOutlet UITextField *confirmChangedPassword;
@property (weak, nonatomic) IBOutlet UIButton *send;

- (IBAction)send:(id)sender;

@end
