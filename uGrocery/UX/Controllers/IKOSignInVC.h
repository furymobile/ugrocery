//
//  IKOSignInViewController.h
//  uGrocery
//
//  Created by Duane Schleen on 7/3/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKOSignInVC : UITableViewController <UITextFieldDelegate>

@property (nonatomic, copy) void (^completionBlock)();

@property (weak, nonatomic) IBOutlet UITextField *emailAddress;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UIButton *signin;
@property (weak, nonatomic) IBOutlet UIButton *cancel;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *signIn;

- (IBAction)signIn:(id)sender;
- (IBAction)cancel:(id)sender;
- (IBAction)forgotPassword:(id)sender;

@end
