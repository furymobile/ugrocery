//
//  IKOUserPreferencesViewController.h
//  uGrocery
//
//  Created by Duane Schleen on 8/7/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKOUserPreferencesViewController : UITableViewController

@end
