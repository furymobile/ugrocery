//
//  IKOForgotPasswordViewController.m
//  uGrocery
//
//  Created by Duane Schleen on 12/30/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOForgotPasswordViewController.h"
#import "UIAlertView+Blocks.h"
#import "UIColor+uGrocery.h"
#import "IKORegex.h"
#import "IKOSDK.h"
#import "IKOError.h"

@interface IKOForgotPasswordViewController ()

@end

@implementation IKOForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    _emailAddress.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated {
    ANALYTIC(@"Opened Forgot Password Screen");
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)viewDidAppear:(BOOL)animated {
    [self performSelector:@selector(becomeFirstResponder) withObject:_emailAddress afterDelay:1];
}

#pragma mark =- UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == _emailAddress) {
        [self send:self];
    }
    return YES;
}

#pragma mark - Validation

- (BOOL)isValid {
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:kValidateEmailRegex options:NSRegularExpressionCaseInsensitive error:nil];
    BOOL validEmail = [regex numberOfMatchesInString:_emailAddress.text options:NSMatchingReportProgress range:NSMakeRange(0, [_emailAddress.text length])] > 0;
    if (!validEmail) {
        [UIAlertView showWithTitle:@"Invalid Email"
        message:@"Please enter a valid Email Address."
        cancelButtonTitle:@"OK"
        otherButtonTitles:nil
        tapBlock:nil];
        return NO;
    }
    return YES;
}

- (IBAction)send:(id)sender {
    if ([self isValid]) {
        [[IKOSDK new]forgotPassword:_emailAddress.text
        callback:^(IKOError *error, IKOServerResponse *response) {
            if (response.success) {
                ANALYTIC(@"Sent User Forgot Password Email");
                [UIAlertView showWithTitle:@"Email Sent"
                message:SF(@"An email with instructions has been sent to %@", _emailAddress.text)
                cancelButtonTitle:@"OK"
                otherButtonTitles:nil
                tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                    if (buttonIndex == 1) {
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                }];
            } else {
                ANALYTIC_ERROR(@"Error Sending Forgot Password Email", error.error);
                [UIAlertView showWithTitle:@"Error Requesting Reset"
                message:SF(@"We were unable to send a password reset email.")
                cancelButtonTitle:@"OK"
                otherButtonTitles:nil
                tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                    if (buttonIndex == 1) {
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                }];
            }
        }];
    }
}

@end
