//
//  IKOProductDetailBaseViewController.m
//  uGrocery
//
//  Created by Duane Schleen on 10/30/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOAuthenticationManager.h"
#import "IKODataStorageManager+uGrocery.h"
#import "IKOImageToastView.h"
#import "IKOLocationManager.h"
#import "IKOProductDetailBaseVC.h"
#import "IKOUtilities.h"
#import "UIScreen+Additions.h"
#import "UIView+Toast.h"
#import "UIViewController+Utilities.h"

@implementation IKOProductDetailBaseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupViewController];

    _settingsDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOObject class]];
    _storeDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOStore class]];
    _priceUpdateDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOPriceUpdate class]];
    _productDataStore = [[IKODataStorageManager sharedInstance] storeForClass:[IKOProduct class]];

    _shoppingListDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOShoppingListItem class]];
    _shoppingListItem = [_shoppingListDataStore getShoppingListItemForProductId:_product.productId];

    _myProductsDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOFavoriteItem class]];
    _myProductItem = [_myProductsDataStore getMyProductItemForProductId:_product.productId];

    _hasEdited = [NSMutableSet new];
    [self initDefaults];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _shoppingListItem = [_shoppingListDataStore getShoppingListItemForProductId:_product.productId];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self saveShoppingListItem];
    [self saveDefaults];
}

- (void)initDefaults {
    if ([_settingsDataStore hasSettingForKey:kLastUsedUpdatedOn]) {
        NSDate *lastUpdated = [_settingsDataStore objectForKey:kLastUsedUpdatedOn];
        NSDate *clearAfter = [lastUpdated dateByAddingTimeInterval:1800]; //30 minutes
        if ([clearAfter compare:[NSDate date]] == NSOrderedDescending) {
            _brand = [_settingsDataStore objectForKey:kLastUsedBrandKey];
            _category = [_settingsDataStore objectForKey:kLastUsedCategoryKey];
            _price = [_settingsDataStore objectForKey:kLastUsedPrice];
            _saleDescription = [_settingsDataStore objectForKey:kLastUsedSaleDescription];
            _saleEndDate = [NSNumber numberWithInteger:[[_settingsDataStore objectForKey:kLastUsedSaleEndDate]integerValue]];
            _salePrice = [_settingsDataStore objectForKey:kLastUsedSalePrice];
            _store = [_settingsDataStore objectForKey:kLastUsedStoreKey];
        } else {
            [_settingsDataStore removeObjectForKey:kLastUsedBrandKey];
            [_settingsDataStore removeObjectForKey:kLastUsedCategoryKey];
            [_settingsDataStore removeObjectForKey:kLastUsedPrice];
            [_settingsDataStore removeObjectForKey:kLastUsedSaleDescription];
            [_settingsDataStore removeObjectForKey:kLastUsedSaleEndDate];
            [_settingsDataStore removeObjectForKey:kLastUsedSalePrice];
            [_settingsDataStore removeObjectForKey:kLastUsedStoreKey];
            [_settingsDataStore removeObjectForKey:kLastUsedUpdatedOn];
        }
    }
}

- (void)saveDefaults {
    if (_store)
        [_settingsDataStore setObject:_store forKey:kLastUsedStoreKey];
    if (_category) {
        [_settingsDataStore setObject:_category forKey:kLastUsedCategoryKey];
    } else {
        [_settingsDataStore removeObjectForKey:kLastUsedCategoryKey];
    }
    if (_brand) {
        [_settingsDataStore setObject:_brand forKey:kLastUsedBrandKey];
    } else {
        [_settingsDataStore removeObjectForKey:kLastUsedBrandKey];
    }
    if (_price) {
        [_settingsDataStore setObject:_price forKey:kLastUsedPrice];
    } else {
        [_settingsDataStore removeObjectForKey:kLastUsedPrice];
    }
    if (_salePrice) {
        [_settingsDataStore setObject:_salePrice forKey:kLastUsedSalePrice];
    } else {
        [_settingsDataStore removeObjectForKey:kLastUsedSalePrice];
    }
    if (_saleDescription) {
        [_settingsDataStore setObject:_saleDescription forKey:kLastUsedSaleDescription];
    } else {
        [_settingsDataStore removeObjectForKey:kLastUsedSaleDescription];
    }
    if (_saleEndDate) {
        [_settingsDataStore setObject:SF(@"%@", _saleEndDate) forKey:kLastUsedSaleEndDate];
    } else {
        [_settingsDataStore removeObjectForKey:kLastUsedSaleEndDate];
    }

    if (_store || _category || _brand || _price || _saleEndDate || _saleDescription || _salePrice)
        [_settingsDataStore setObject:[NSDate date] forKey:kLastUsedUpdatedOn];
}

- (BOOL)saveShoppingListItem {
    IKOShoppingListItem *item = _shoppingListItem;
    if (_shoppingListItem) {
        BOOL added = [_shoppingListDataStore saveShoppingListItem:&item];
        if (added && _myProductItem) {
            IKOFavoriteItem *myProductItem = _myProductItem;
            myProductItem.l1Id = item.l1Id;
            [_myProductsDataStore saveMyProductItem:&myProductItem];
        } else if (added) {
            IKOFavoriteItem *myProductItem = [[IKOFavoriteItem alloc]initWithProduct:self.product];
            myProductItem.l1Id = _shoppingListItem.l1Id;
            [_myProductsDataStore saveMyProductItem:&myProductItem];
        }
    }
    return NO;
}

- (void)sendProductUpdate:(IKOPriceUpdate *)submission {
    submission.storeId = _store.storeId;
    NSString *tempPrice = [[self.price componentsSeparatedByCharactersInSet:
      [[NSCharacterSet characterSetWithCharactersInString:@".0123456789"]
      invertedSet]]
      componentsJoinedByString:@""];
    submission.price = [NSNumber numberWithFloat:[tempPrice floatValue]];
    submission.size =  _size;
    submission.latitude = [NSNumber numberWithDouble:[[IKOLocationManager sharedInstance]getCurrentLocation].coordinate.latitude];
    submission.longitude = [NSNumber numberWithDouble:[[IKOLocationManager sharedInstance]getCurrentLocation].coordinate.longitude];
    submission.saleEndDate = _saleEndDate;
    tempPrice = [[self.salePrice componentsSeparatedByCharactersInSet:
      [[NSCharacterSet characterSetWithCharactersInString:@".0123456789"]
      invertedSet]]
      componentsJoinedByString:@""];
    submission.salePrice = [NSNumber numberWithFloat:[tempPrice floatValue]];
    submission.saleDescription = self.saleDescription;
    submission.ppfSafe = [NSNumber numberWithBool:self.ppfSafe];

    [_priceUpdateDataStore sendPriceUpdate:submission];
}

- (void)showToastWithImageView:(UIImageView *)image andMessage:(NSString *)message {
    IKOImageToastView *toast = [[IKOImageToastView alloc]initWithFrame:CGRectMake(20, 100, 220, 48) andImage:image.image andMessage:message];
    [self.view.superview showToast:toast duration:3 position:[NSValue valueWithCGPoint:CGPointMake([UIScreen currentSize].width / 2, [UIScreen currentSize].height - (self.navigationController.toolbar.frame.size.height * 2))]];
}

#pragma mark - IKOAddToShoppingListProtocol

- (void)addToShoppingList:(id)object {
    if ([object isKindOfClass:[IKOProduct class]]) {
        IKOProduct *product = (IKOProduct *)object;
        _shoppingListItem = [[IKOShoppingListItem alloc]initWithProduct:product];
        [self saveShoppingListItem];
        NSDictionary *params = @{@"Product ID": product.productId ? : @"", @"Product Description": product.productDescription ? : @"", @"UPC": product.upc ? : @"", @"UPC-E": product.upce ? : @""};
        ANALYTIC_PARAMS(@"Added Product to Shopping List", params);
        [self.tableView reloadData];
    }
}

- (void)quantityChanged:(NSNumber *)quantity {
    _shoppingListItem.quantity = quantity;
    [self saveShoppingListItem];
}

- (void)colorText:(UITextField *)textField forObject:(id)obj {
    if (obj == nil) {
        textField.textColor = [UIColor blackColor];
        return;
    }
    textField.textColor = ([self.hasEdited containsObject:[NSNumber numberWithInteger:textField.tag]]) ?[UIColor blackColor] :[UIColor redColor];
}

- (BOOL)currencyTextField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if ([string isEqualToString:@"."])
        return NO;

    if (string.length == 0)
        return YES;

    NSInteger MAX_DIGITS = 11; // $999,999,999.99

    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc]init];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numberFormatter setMaximumFractionDigits:2];
    [numberFormatter setMinimumFractionDigits:2];

    NSString *stringMaybeChanged = [NSString stringWithString:string];
    if (stringMaybeChanged.length > 1) {
        NSMutableString *stringPasted = [NSMutableString stringWithString:stringMaybeChanged];

        [stringPasted replaceOccurrencesOfString:numberFormatter.currencySymbol
        withString:@""
        options:NSLiteralSearch
        range:NSMakeRange(0, [stringPasted length])];

        [stringPasted replaceOccurrencesOfString:numberFormatter.groupingSeparator
        withString:@""
        options:NSLiteralSearch
        range:NSMakeRange(0, [stringPasted length])];

        NSDecimalNumber *numberPasted = [NSDecimalNumber decimalNumberWithString:stringPasted];
        stringMaybeChanged = [numberFormatter stringFromNumber:numberPasted];
    }

    UITextRange *selectedRange = [textField selectedTextRange];
    UITextPosition *start = textField.beginningOfDocument;
    NSInteger cursorOffset = [textField offsetFromPosition:start toPosition:selectedRange.start];
    NSMutableString *textFieldTextStr = [NSMutableString stringWithString:textField.text];
    NSUInteger textFieldTextStrLength = textFieldTextStr.length;

    [textFieldTextStr replaceCharactersInRange:range withString:stringMaybeChanged];

    [textFieldTextStr replaceOccurrencesOfString:numberFormatter.currencySymbol
    withString:@""
    options:NSLiteralSearch
    range:NSMakeRange(0, [textFieldTextStr length])];

    [textFieldTextStr replaceOccurrencesOfString:numberFormatter.groupingSeparator
    withString:@""
    options:NSLiteralSearch
    range:NSMakeRange(0, [textFieldTextStr length])];

    [textFieldTextStr replaceOccurrencesOfString:numberFormatter.decimalSeparator
    withString:@""
    options:NSLiteralSearch
    range:NSMakeRange(0, [textFieldTextStr length])];

    if (textFieldTextStr.length <= MAX_DIGITS) {
        NSDecimalNumber *textFieldTextNum = [NSDecimalNumber decimalNumberWithString:textFieldTextStr];
        NSDecimalNumber *divideByNum = [[[NSDecimalNumber alloc]initWithInt:10]decimalNumberByRaisingToPower:numberFormatter.maximumFractionDigits];
        NSDecimalNumber *textFieldTextNewNum = [textFieldTextNum decimalNumberByDividingBy:divideByNum];
        NSString *textFieldTextNewStr = [numberFormatter stringFromNumber:textFieldTextNewNum];

        self.price = textField.text = textFieldTextNewStr;

        if (cursorOffset != textFieldTextStrLength) {
            NSInteger lengthDelta = textFieldTextNewStr.length - textFieldTextStrLength;
            NSInteger newCursorOffset = MAX(0, MIN(textFieldTextNewStr.length, cursorOffset + lengthDelta));
            UITextPosition *newPosition = [textField positionFromPosition:textField.beginningOfDocument offset:newCursorOffset];
            UITextRange *newRange = [textField textRangeFromPosition:newPosition toPosition:newPosition];
            [textField setSelectedTextRange:newRange];
        }
    }

    return NO;
}

- (BOOL)showPriceCapture {
    return [_settingsDataStore hasSettingForKey:kShowAdvancedPriceCapture] ?[[_settingsDataStore objectForKey:kShowAdvancedPriceCapture]boolValue] : NO;
}

@end
