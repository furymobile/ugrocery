//
//  IKORegisterUserViewController.h
//  uGrocery
//
//  Created by Duane Schleen on 8/5/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKORegisterUserVC : UITableViewController <UITextFieldDelegate>

@property (nonatomic, copy) void (^completionBlock)();

@property (weak, nonatomic) IBOutlet UITextField *emailAddress;
@property (weak, nonatomic) IBOutlet UITextField *zipCode;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UIButton *signup;
@property (weak, nonatomic) IBOutlet UIButton *cancel;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *submitRegistration;

- (IBAction)submitRegistration:(id)sender;
- (IBAction)cancel:(id)sender;

@end
