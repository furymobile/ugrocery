//
//  IKOZeroStateOverlay.h
//  uGrocery
//
//  Created by Duane Schleen on 8/26/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKOZeroStateOverlay : UIView

- (id)initWithFrame:(CGRect)frame message:(NSString *)message;

@end
