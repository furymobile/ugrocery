//
//  IKOSplitView.h
//  uGrocery
//
//  Created by Duane Schleen on 11/29/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKOSplitView : UIView

@property (nonatomic, readonly, retain) UIView *topView;
@property (nonatomic, readonly, retain) UIView *bottomView;

- (instancetype)initWithBottomView:(UIView *)view andTopView:(UIView *)topView;

- (void)hideTopView:(BOOL)animated;
- (void)expandTopViewTo:(CGFloat)height animated:(BOOL)animated;
- (void)collpaseTopViewTo:(CGFloat)height animated:(BOOL)animated;

@end
