//
//  IKOZeroStateOverlay.m
//  uGrocery
//
//  Created by Duane Schleen on 8/26/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOZeroStateOverlay.h"
#import "UIFont+uGrocery.h"
#import "UIColor+uGrocery.h"

@implementation IKOZeroStateOverlay {
    NSString *_message;
}

- (id)initWithFrame:(CGRect)frame message:(NSString *)message {
    self = [super initWithFrame:frame];
    if (self) {
        _message = message;
        [self buildUI];
    }
    return self;
}

- (void)buildUI {
    self.backgroundColor = [UIColor whiteColor];
    NSMutableParagraphStyle *style =  [[NSParagraphStyle defaultParagraphStyle]mutableCopy];
    style.alignment = NSTextAlignmentCenter;
    style.firstLineHeadIndent = 10.0f;
    style.headIndent = 10.0f;
    style.tailIndent = 10.0f;

    // Create the attributed string
    NSMutableAttributedString *myString = [[NSMutableAttributedString alloc]initWithString:
      @"Title\n\nLorem ipsum yada yada"];

    // Declare the fonts
    UIFont *myStringFont1 = [UIFont fontWithName:@"Roboto-Regular" size:14.0];
    UIFont *myStringFont2 = [UIFont fontWithName:@"Roboto-Light" size:14.0];

    // Declare the colors
    UIColor *myStringColor1 = [UIColor UGBlue];
    UIColor *myStringColor2 = [UIColor UGDkGrey];


    // Create the attributes and add them to the string
    [myString addAttribute:NSForegroundColorAttributeName value:myStringColor1 range:NSMakeRange(0,7)];
    [myString addAttribute:NSParagraphStyleAttributeName value:style range:NSMakeRange(0,7)];
    [myString addAttribute:NSFontAttributeName value:myStringFont1 range:NSMakeRange(0,7)];

    [myString addAttribute:NSForegroundColorAttributeName value:myStringColor2 range:NSMakeRange(7,21)];
    [myString addAttribute:NSParagraphStyleAttributeName value:style range:NSMakeRange(7,21)];
    [myString addAttribute:NSFontAttributeName value:myStringFont2 range:NSMakeRange(7,21)];

    UILabel *label = [[UILabel alloc]initWithFrame:self.frame];
    label.attributedText = myString;

    [self addSubview:label];
}

@end
