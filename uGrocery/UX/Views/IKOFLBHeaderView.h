//
//  IKOFLBHeaderView.h
//  uGrocery
//
//  Created by Duane Schleen on 8/18/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol IKOFLBHeaderDelegate <NSObject>

@required

- (void)selectedStoreId:(NSString *)storeId;
- (void)findLowestBill;
- (void)launchStore;

@end

@class IKOFLBHelper;

@interface IKOFLBHeaderView : UIView

@property (nonatomic) BOOL isExpanded;
@property (nonatomic) id<IKOFLBHeaderDelegate> delegate;
@property (nonatomic) UIView *overlaysView;
@property (nonatomic) UIViewController *currentViewController;

- (void)resetHeaderView;
- (void)setFrameToStoreId:(NSString *)storeId;
- (void)configureWithFLBHelper:(IKOFLBHelper *)helper;
- (void)expandView;
- (void)collapse;

@end
