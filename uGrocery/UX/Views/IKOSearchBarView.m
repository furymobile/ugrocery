//
//  IKOSearchBarView.m
//  uGrocery
//
//  Created by Duane Schleen on 7/28/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOSearchBarView.h"
#import "UIColor+uGrocery.h"
#import "UIImage+Additions.h"
#import "UIScreen+Additions.h"

@implementation IKOSearchBarView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self buildUI];
    }
    return self;
}

- (void)buildUI {
    self.backgroundColor = [UIColor whiteColor];
    //self.alpha = 0.89;

    _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, [UIScreen currentSize].width - 50, 44)];
    _searchBar.placeholder = @"search";
    _searchBar.searchBarStyle = UISearchBarStyleProminent;
    _searchBar.translucent = YES;
    _searchBar.backgroundImage = [UIImage imageFromColor:[UIColor UGWhite]];

    UIView *blueSquare = [[UIView alloc]initWithFrame:CGRectMake([UIScreen currentSize].width - 44, 0, 44, 44)];
    blueSquare.backgroundColor = [UIColor UGBlue];
    
    UIView *underline = [[UIView alloc]initWithFrame:CGRectMake(35, 35,[UIScreen currentSize].width - 100, 1)];
    underline.backgroundColor = [UIColor UGReallyLtGrey];
    
    _scanBarcode = [[UIButton alloc]initWithFrame:CGRectMake([UIScreen currentSize].width - 44, 0, 44, 44)];
    [_scanBarcode setImage:[UIImage imageNamed:@"icon-barcode"] forState:UIControlStateNormal];
    [_scanBarcode addTarget:self action:@selector(scanBarCode:) forControlEvents:UIControlEventTouchUpInside];
    _scanBarcode.backgroundColor = [UIColor clearColor];

    [self addSubview:_triggerProductSearch];
    [self addSubview:blueSquare];
    [self addSubview:_scanBarcode];
    [self addSubview:_searchBar];
    [self addSubview:underline];
}

- (IBAction)scanBarCode:(id)sender {
    [_searchBar resignFirstResponder];
    [_delegate scanBarcode];
}

- (IBAction)triggerProductSearch:(id)sender {
    [_searchBar resignFirstResponder];
    [_delegate triggerProductSearchFor:_searchBar.text];
}

@end
