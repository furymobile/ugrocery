//
//  IKOSplitView.m
//  uGrocery
//
//  Created by Duane Schleen on 11/29/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOSplitView.h"

@interface IKOSplitView ()



@end

@implementation IKOSplitView

- (instancetype)initWithBottomView:(UIView *)view andTopView:(UIView *)topView {
    self = [super initWithFrame:[[UIScreen mainScreen]bounds]];
    if (self) {
        [_bottomView removeFromSuperview];
        [_topView removeFromSuperview];
        _bottomView = view;
        _topView = view;
        [self addSubview:_bottomView];
        [self addSubview:_topView];
        [self setNeedsLayout];
    }
    return self;
}

- (void)layoutSubviews {
    _topView.frame = CGRectMake(0, 0, self.frame.size.width, _topView.frame.size.height);
    _bottomView.frame = CGRectMake(0, _topView.frame.size.height, self.frame.size.width, self.frame.size.height - _topView.frame.size.height);
}

- (void)hideTopView:(BOOL)animated {
    [self setTopFrame:CGRectMake(0, 0, self.frame.size.width, 0) animated:animated];
}

- (void)expandTopViewTo:(CGFloat)height animated:(BOOL)animated {
    [self setTopFrame:CGRectMake(0, 0, self.frame.size.width, height) animated:animated];
}

- (void)collpaseTopViewTo:(CGFloat)height animated:(BOOL)animated {
    [self setTopFrame:CGRectMake(0, 0, self.frame.size.width, height) animated:animated];
}

- (void)setTopFrame:(CGRect)frame animated:(BOOL)animated {
    if (animated) {
        [UIView animateWithDuration:0.3f animations:^{
            _topView.frame = frame;
        }];
        return;
    }
    _topView.frame = frame;
}

@end
