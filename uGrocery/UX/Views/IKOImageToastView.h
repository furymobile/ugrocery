//
//  IKOImageToastView.h
//  uGrocery
//
//  Created by Duane Schleen on 8/3/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKOImageToastView : UIView

- (id)initWithFrame:(CGRect)frame
  andImage:(UIImage *)image
  andMessage:(NSString *)message;

@end
