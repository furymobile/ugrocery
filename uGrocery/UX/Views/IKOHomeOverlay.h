//
//  IKOHomeOverlay.h
//  uGrocery
//
//  Created by Duane Schleen on 8/4/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKOHomeOverlay : UIView

@property (nonatomic) IBOutlet UIButton *signIn;
@property (nonatomic) IBOutlet UIButton *signUp;
@property (nonatomic) IBOutlet UIButton *support;

- (void)setIsAuthenticated:(BOOL)authenticated;

@end
