//
//  IKODealsHeaderView.h
//  uGrocery
//
//  Created by Duane Schleen on 8/18/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol IKODealsHeaderDelegate <NSObject>

@required

- (void)selectedStoreId:(NSString *)storeId;

@end

@interface IKODealsHeaderView : UIView

@property (nonatomic) BOOL isExpanded;
@property (nonatomic) id<IKODealsHeaderDelegate> delegate;
@property (nonatomic) UIView *overlaysView;

- (void)setFrameToStoreId:(NSString *)storeId;
- (void)expandView;

@end
