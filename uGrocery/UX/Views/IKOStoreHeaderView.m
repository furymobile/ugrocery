//
//  IKOCloseHeaderView.m
//  uGrocery
//
//  Created by Duane Schleen on 9/2/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOStoreHeaderView.h"
#import "UIColor+uGrocery.h"
#import "UIScreen+Additions.h"
#import "UIFont+uGrocery.h"

@interface IKOStoreHeaderView ()

@property (nonatomic) UIImage *image;
@property (nonatomic) NSString *storeName;

@end

@implementation IKOStoreHeaderView

- (id)initWithFrame:(CGRect)frame andImage:(UIImage *)image andStoreName:(NSString *)storeName {
    self = [super initWithFrame:frame];
    if (self) {
        self.image = image;
        self.storeName = storeName;
        [self buildUI];
    }
    return self;
}

- (void)buildUI {
    self.backgroundColor = [UIColor whiteColor];

    _closeButton = [UIButton buttonWithType:UIButtonTypeSystem];
    _closeButton.frame = CGRectMake(260, 7, 40, 30);
    [_closeButton setTitle:@"Close" forState:UIControlStateNormal];

    UIImageView *imageView = [[UIImageView alloc]initWithImage:_image];
    [imageView setContentMode:UIViewContentModeScaleAspectFit];
    imageView.frame = CGRectMake(9, 6, 30, 30);

    UILabel *storeName = [[UILabel alloc]init];
    storeName.text = _storeName;
    storeName.font = [UIFont IKOLightFontOfSize:17];
    storeName.frame = CGRectMake(45, 0, 200, 42);
    storeName.textColor = [UIColor IKODarkGrayColor];

    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0, 43, [UIScreen currentSize].width, 1)];
    line.backgroundColor = [UIColor IKODividerColor];

    [self addSubview:imageView];
    [self addSubview:storeName];
    //   [self addSubview:_closeButton];
    [self addSubview:line];
}

@end
