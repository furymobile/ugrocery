//
//  PushNoAnimationSegue.h
//  Copyright (c) 2014 Fury Mobile. All rights reserved.
//
//  License:
//
//  Apache License
//  Version 2.0, January 2004
//  http://www.apache.org/licenses/
//

#import <UIKit/UIKit.h>

@interface PushNoAnimationSegue : UIStoryboardSegue

@end
