//
//  IKOSubscriptionCell.h
//  uGrocery
//
//  Created by Duane Schleen on 7/26/15.
//  Copyright (c) 2015 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SKProduct;

@protocol IKOPurchaseProductDelegate <NSObject>

@required

- (void)purchaseProduct:(SKProduct *)product;

@end

@interface IKOSubscriptionCell : UITableViewCell

@property (nonatomic) id<IKOPurchaseProductDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *productName;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UIButton *subscriptionButton;

- (void)configureCellWith:(SKProduct *)product;
- (IBAction)makePurchase:(id)sender;

@end
