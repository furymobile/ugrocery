//
//  SwitchCell.swift
//  uGrocery
//
//  Created by Duane Schleen on 10/16/16.
//  Copyright © 2016 ikonomo. All rights reserved.
//

import UIKit

@objc class SwitchCell: UITableViewCell {
    
    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var cellSwitch: UISwitch!
    
}
