//
//  MainNavigationCell.swift
//  uGrocery
//
//  Created by Duane Schleen on 10/16/16.
//  Copyright © 2016 ikonomo. All rights reserved.
//

import UIKit

@objc class MainNavigationCell: UITableViewCell {
    
    @IBOutlet weak var scanButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var browseButton: UIButton!
    
}
