//
//  IKOSpinnerViewCell.m
//  uGrocery
//
//  Created by Duane Schleen on 9/16/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOSpinnerViewCell.h"
#import "UIView+Animation.h"

@implementation IKOSpinnerViewCell

- (void)runSpinner {
    [_spinnerView spinWithDuration:15 rotations:1 repeat:10000];
}

@end
