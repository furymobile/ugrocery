//
//  IKOSpinnerViewCell.h
//  uGrocery
//
//  Created by Duane Schleen on 9/16/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKOSpinnerViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *spinnerView;

- (void)runSpinner;

@end
