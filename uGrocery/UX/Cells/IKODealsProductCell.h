//
//  IKODealsProductCell.h
//  uGrocery
//
//  Created by Duane Schleen on 9/5/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IKOProduct;

@protocol IKODealProductSelectionDelegate <NSObject>

@required

- (void)selectProduct:(IKOProduct *)product selected:(BOOL)selected;
- (void)annimateAddToShoppingList:(UIImageView *)imageView;

@end

@interface IKODealsProductCell : UITableViewCell

@property (nonatomic) id<IKODealProductSelectionDelegate> selectionDelegate;

@property (weak, nonatomic) IBOutlet UIView *quantityView;
@property (weak, nonatomic) IBOutlet UIImageView *productImage;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *discount;

- (void)configureWith:(IKOProduct *)product andProductNode:(NSDictionary *)productNode;

- (IBAction)selectedTapped:(id)sender;
- (void)selectItem:(BOOL)selected;

@end
