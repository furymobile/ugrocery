//
//  UserCell.swift
//  uGrocery
//
//  Created by Duane Schleen on 10/16/16.
//  Copyright © 2016 ikonomo. All rights reserved.
//

import UIKit

@objc class UserCell: UITableViewCell {
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userEmail: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    
}
