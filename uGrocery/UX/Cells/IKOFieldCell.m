//
//  IKOFieldCell.m
//  uGrocery
//
//  Created by Duane Schleen on 10/30/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOFieldCell.h"

@implementation IKOFieldCell

- (void)configureWithLabel:(NSString *)label andValue:(NSString *)value andKey:(NSString *)key {
    _key = key;
    _fieldLabel.text = [label uppercaseString];
    _textField.text = value;
}

- (void)showClearButton {
    _clear.hidden = NO;
    [_clear addTarget:self action:@selector(clearTextField:) forControlEvents:UIControlEventTouchUpInside];
}

- (IBAction)clearTextField:(id)sender {
    _textField.text = nil;
    if (_delegate)
        [_delegate textFieldDidClear:_textField];
}

- (void)prepareForReuse {
    _clear.hidden = YES;
}

@end
