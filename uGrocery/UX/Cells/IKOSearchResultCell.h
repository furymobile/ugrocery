//
//  IKOSearchResultCell.h
//  uGrocery
//
//  Created by Duane Schleen on 7/24/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IKOSearchItem.h"
#import "IKOShoppingListDataStore.h"
#import "IKOAddToShoppingListProtocol.h"

@interface IKOSearchResultCell : UITableViewCell

@property (nonatomic) id<IKOAddToShoppingListProtocol> delegate;

@property (weak, nonatomic) IBOutlet UIButton *addToShoppingList;
@property (weak, nonatomic) IBOutlet UILabel *descriptionText;
@property (weak, nonatomic) IBOutlet UIImageView *resultImage;
@property (weak, nonatomic) IBOutlet UIView *quantityView;
@property (weak, nonatomic) IBOutlet UITextField *quantityField;
@property (weak, nonatomic) IBOutlet UIStepper *quantityStepper;
@property (weak, nonatomic) IBOutlet UIButton *markFavorite;
@property (weak, nonatomic) IBOutlet UILabel *discountText;
@property (weak, nonatomic) IBOutlet UILabel *measure;

@property (weak, nonatomic) IBOutlet UILabel *maxSavings;
@property (weak, nonatomic) IBOutlet UIImageView *storeLogo;

@property (nonatomic, setter = setQuantity :) NSNumber *quantity;
@property (weak, nonatomic) IBOutlet UILabel *productId;

- (void)configureWithSearchItem:(IKOSearchItem *)item;
- (void)configureWithSearchItem:(IKOSearchItem *)item andCurrentSearchTerm:(NSString *)term;
- (void)configureWithProduct:(IKOProduct *)product;
- (void)configurewWithAdhocProduct:(IKOProductCategory *)category;

- (void)setFavorite:(BOOL)favorite;
- (void)selectItem:(BOOL)selected;

- (IBAction)addToShoppingList:(id)sender;
- (IBAction)quantityStepper:(id)sender;
- (IBAction)markFavorite:(id)sender;


@end
