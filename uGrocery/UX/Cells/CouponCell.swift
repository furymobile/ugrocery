//
//  CouponCell.swift
//  uGrocery
//
//  Created by Duane Schleen on 10/16/16.
//  Copyright © 2016 ikonomo. All rights reserved.
//

import UIKit

@objc class CouponCell: UITableViewCell {
    
    @IBOutlet weak var discount: UILabel!
    @IBOutlet weak var source: UILabel!
    @IBOutlet weak var expiration: UILabel!
    @IBOutlet weak var minimumQuantity: UILabel!
    @IBOutlet weak var couponDescription: UILabel!
    
}
