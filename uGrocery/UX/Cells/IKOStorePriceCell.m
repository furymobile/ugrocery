//
//  IKOStorePriceCell.m
//  uGrocery
//
//  Created by Duane Schleen on 7/28/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOStorePriceCell.h"
#import "UIColor+uGrocery.h"
#import "UIScreen+Additions.h"

@implementation IKOStorePriceCell

- (void)setImages:(BOOL)onSale hasCoupons:(BOOL)hasCoupons {
    _onSale = onSale;
    _hasCoupons = hasCoupons;

    CGSize expectedLabelSize = [_price.text boundingRectWithSize:_price.frame.size
      options:NSStringDrawingUsesLineFragmentOrigin
      attributes:@{
          NSFontAttributeName: _price.font
      }
      context:nil].size;

    int factor = (onSale & hasCoupons) ? 2 : 1;
    UIView *containerView = [[UIView alloc]initWithFrame:CGRectMake([UIScreen currentSize].width - 40 - expectedLabelSize.width - (factor * 25), 7, (factor * 25), 25)];
    if (onSale) {
        UIImageView *saleImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"icon-sale"]];
        saleImage.frame = CGRectMake(0, 0, 25, 25);
        [containerView addSubview:saleImage];
    }
    if (hasCoupons) {
        UIImageView *couponImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"icon-coupon"]];
        couponImage.frame = CGRectMake(25* (factor-1), 0, 25, 25);
        [containerView addSubview:couponImage];
    }
    [self addSubview:containerView];
}

@end
