//
//  IKOShoppingListCell.h
//  uGrocery
//
//  Created by Duane Schleen on 7/18/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IKOShoppingListItem.h"
#import "SWRevealTableViewCell.h"

@protocol IKOCrossOffShoppingListItemProtocol <NSObject>

@required

- (void)crossOff:(IKOShoppingListItem *)item crossed:(BOOL)isCrossed sender:(id)sender;

@end


@interface IKOShoppingListCell : SWRevealTableViewCell

@property (nonatomic) id<IKOCrossOffShoppingListItemProtocol> crossOffDelegate;

@property (weak, nonatomic) IBOutlet UILabel *productDescription;
@property (weak, nonatomic) IBOutlet UIView *quantityView;
@property (weak, nonatomic) IBOutlet UIImageView *productImage;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *measure;
@property (weak, nonatomic) IBOutlet UILabel *notComparable;

- (void)configureWith:(IKOShoppingListItem *)item;
- (void)configureWithPrice:(NSString *)price;

- (IBAction)crossOff:(id)sender;

@end
