//
//  IKOMyProductCell.m
//  uGrocery
//
//  Created by Duane Schleen on 7/18/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOMyProductCell.h"
#import "UIColor+uGrocery.h"
#import "UIFont+uGrocery.h"
#import "Haneke.h"
#import "IKOFavoriteItem.h"
#import "IKOProductDataStore.h"
#import "IKODataStorageManager+uGrocery.h"

@implementation IKOMyProductCell {
    IKOFavoriteItem *_item;
    UIImageView *_circleImage;
    BOOL _selected;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        if (NSFoundationVersionNumber <= NSFoundationVersionNumber_iOS_7_1) {
            UIView *cellContentView = self.contentView;
            cellContentView.translatesAutoresizingMaskIntoConstraints = NO;

            [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[cellContentView]|"
            options:0
            metrics:0
            views:NSDictionaryOfVariableBindings(cellContentView)]];
            [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[cellContentView]|"
            options:0
            metrics:0
            views:NSDictionaryOfVariableBindings(cellContentView)]];
        }
    }
    return self;
}

- (void)configureWith:(IKOFavoriteItem *)item {
    _item = item;

    if (item.imageUri)
        [_productImage hnk_setImageFromURL:[NSURL URLWithString:SF(kProductImageUrl, item.imageUri, item.productId)] placeholder:[UIImage imageNamed:@"noproductimage"]];
    [_markFavorite setImage:([_item.isFavorite boolValue]) ?[UIImage imageNamed:@"icon-heart-selected"] : nil];
    _descriptionLabel.text = item.productDescription;
    _measure.text = item.measureText;
    
}

- (void)configurewith:(IKOProduct *)product {
    if ([product.maxSavings intValue] > 0) {
        self.maxSavings.hidden = NO;
        self.storeLogo.hidden = NO;
        
        IKOStoreDataStore *storeDataStore = [[IKODataStorageManager sharedInstance] storeForClass:[IKOStore class]];
        
        NSMutableAttributedString *saleText = [[NSMutableAttributedString alloc]initWithString:SF(@"SAVE UP TO %d%%", [product.maxSavings intValue])];
        NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc]init];
        paraStyle.alignment = NSTextAlignmentRight;
        [saleText addAttribute:NSParagraphStyleAttributeName value:paraStyle range:NSMakeRange(0, saleText.length)];
        [saleText addAttribute:NSForegroundColorAttributeName value:[UIColor IKODarkGrayColor] range:NSMakeRange(0,10)];
        [saleText addAttribute:NSForegroundColorAttributeName value:[UIColor UGGreen] range:NSMakeRange(11,saleText.length-11)];
        [saleText addAttribute:NSFontAttributeName value:[UIFont IKOMediumFontOfSize:15] range:NSMakeRange(0,10)];
        [saleText addAttribute:NSFontAttributeName value:[UIFont IKOMediumFontOfSize:18] range:NSMakeRange(11,saleText.length-11)];
        
        self.maxSavings.attributedText = saleText;
        self.storeLogo.image = [storeDataStore largerStoreImageFor:product.winningStore];
        
    }
}

- (void)selectItem:(BOOL)selected {
    _selected = selected;

    if (_circleImage)[_circleImage removeFromSuperview];

    _circleImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    _circleImage.image = selected ?[UIImage imageNamed:@"checkbox-checked"] :[UIImage imageNamed:@"addbox"];

    [_quantityView addSubview:_circleImage];
}

- (void)prepareForReuse {
    _item = nil;
    _storeLogo.image = nil;
    _maxSavings.text = nil;
    _selected = NO;
    _productImage.image = [UIImage imageNamed:@"noproductimage"];
    _descriptionLabel.text = nil;
    [_quantityView.subviews enumerateObjectsUsingBlock:^(UIView *obj, NSUInteger idx, BOOL *stop) {
        [obj removeFromSuperview];
    }];
}

- (IBAction)selectedTapped:(id)sender {
    _selected = !_selected;
    [self selectItem:_selected];
    if (_selected)
        [_selectionDelegate annimateAddToShoppingList:_productImage];
    [_selectionDelegate selectMyProduct:_item selected:_selected];
}

@end
