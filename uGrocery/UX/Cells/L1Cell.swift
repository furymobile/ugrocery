//
//  L1Cell.swift
//  uGrocery
//
//  Created by Duane Schleen on 10/16/16.
//  Copyright © 2016 ikonomo. All rights reserved.
//

import UIKit

@objc class  L1Cell: UITableViewCell {
    
    @IBOutlet weak var categoryShadow: UILabel!
    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var categoryImage: UIImageView!
    
}
