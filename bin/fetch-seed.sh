#!/bin/bash

#---------------------------------------------------
# Functions
#---------------------------------------------------
function url
{
    echo "${sSeedUrl}${1}&target=${sTarget}"
}

function ik_curl
{
    type="$1"
    url=$(url "$type")
    echo "Fetching $url via curl"
    rm uGrocery/json/"$type".json
    curl -u "$AUTH_USER":"$AUTH_PASS" -o uGrocery/json/"$type".json "$url"
}

function usage
{
    echo 'fetch-seed.sh <dev|staging|uat|prod> [public|admin]'
    echo 'The first argument is require and specifies the environment'
    echo 'The second argument is option and specifies the seed type; the default is public'
    exit 0
}

#---------------------------------------------------
# Bail if cwd isn't the root of the project
#---------------------------------------------------
if [ ! -d uGrocery/json ]; then
    echo 'Please run the script from the base directory of the project'
    exit 1
fi

#---------------------------------------------------
# Gather the arguments
#---------------------------------------------------

# Bail if first arg is not provided
if [ -z "$1" ]; then
    usage
fi
sEnv="$1"

# Second arg is optional, default is public
sTarget='public'
if [ -n "$2" -a "$2" == 'admin' ]; then
    sTarget='admin'
fi

case "$sEnv" in
    'dev')
        sSeedUrl='http://admin.dev.ikonomo.com/dump-json.php?type=';
    ;;
    'staging')
        sSeedUrl='https://api.staging.ugrocery.com/dump-json.php?type=';
    ;;
    'uat')
        sSeedUrl='https://api.uat.ugrocery.com/dump-json.php?type=';
    ;;
    'prod')
        sSeedUrl='https://api.ugrocery.com/dump-json.php?type=';
    ;;
    *)
        echo "Unknown environment $sEnv provided"
        usage
esac

echo "Proceeding with $sTarget seed in $sEnv environment"

# Pseudo constants for username & pass
AUTH_USER='ik-exporter'
AUTH_PASS='?3QVBPLC2@y*E7=('

#---------------------------------------------------
# Run the script
#---------------------------------------------------
ik_curl brands
ik_curl categories
ik_curl category_joins
ik_curl products
ik_curl zips
