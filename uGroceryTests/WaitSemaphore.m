
#import "WaitSemaphore.h"

@implementation WaitSemaphore

+ (WaitSemaphore *)sharedInstance
{
	static WaitSemaphore *sharedInstance = nil;
	static dispatch_once_t once;

	dispatch_once(&once, ^{
		sharedInstance = [WaitSemaphore alloc];
		sharedInstance = [sharedInstance init];
	});

	return sharedInstance;
}

- (id)init
{
	self = [super init];
	if (self != nil) {
		_flags = [NSMutableDictionary dictionaryWithCapacity:10];
	}
	return self;
}

- (void)dealloc
{
	_flags = nil;
}

- (BOOL)isLifted:(NSString *)key
{
	return [_flags objectForKey:key] != nil;
}

- (void)lift:(NSString *)key
{
	[_flags setObject:@"YES" forKey:key];
}

- (void)waitForKey:(NSString *)key
{
	BOOL keepRunning = YES;
	while (keepRunning && [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:1.0]]) {
		keepRunning = ![[WaitSemaphore sharedInstance] isLifted:key];
	}

}

- (void)purgeKeys
{
	[_flags removeAllObjects];
}

- (void)purgeKey:(NSString *)key
{
	[_flags removeObjectForKey:key];
}

@end
